# Skyfrog

## Übersicht
Skyfrog ist eine im Rahmen des Pro2E entstandene Software, welche dazu dient die während des Fluges des IKURA-Segelfliegers entstandenen CSV-Datensätze auszulesen, zu filtern und den Flug in einer 3D-Ansicht zu visualisieren. Die Messgrössen wie Längengrad, Breitengrad, Flughöhe, usw. sind dabei zu jedem Zeitpunkt in einem Informationspanel ersichtlich und können auf Knopfdruck gegen die Zeit geplottet werden.

## Inhalt
- [Installation](#installation)
- [Erste Schritte](#erste-schritte)
- [Verwendung](#verwendung)
- [Benutzerhandbuch](#benutzerhandbuch)
- [Entwicklerdokumentation](#entwicklerdokumentation)
- [Technische Details](#technische-details)
- [Problemlösung und Support](#problemlösung-und-support)
- [Lizenz](#lizenz)

## Installation
Zum jetzigen Zeitpunkt kann Skyfrog nur über eine Entwicklungsumgebung wie Eclipse IDE ausgeführt werden. Befolgen Sie folgende Schritte, um Skyfrog in Ihre Entwicklungsumgebung (IDE) zu importieren. Sie können das Projekt entweder über Git klonen oder herkömmlich als Archiv herunterladen und lokal importieren.

### Klonen des Projekts:

1. Gehen Sie zu `https://gitlab.fhnw.ch/team_froggo/template-segelflug`
2. Unter dem Dropdown-Menu `"Code"`, klicken Sie die Schaltfläche um die URL zu kopieren. Wählen Sie dabei die URL unter `"Mit SSH klonen"`, falls Sie einen SSH-Schlüssel besitzen. Andernfalls wählen Sie die Schaltfläche unter "Mit HTTPS klonen".
3. Klonen Sie das Projekt in Ihre Arbeitsumgebung.
4. Importieren Sie den geklonten Projektordner von Ihrer Arbeitsumgebung in Ihre IDE.

### Importieren als Ordner:

1. Gehen Sie zu `https://gitlab.fhnw.ch/team_froggo/template-segelflug`
2. Unter dem Dropdown-Menu `"Code"`, klicken Sie die Schaltfläche `"zip"` unter dem Reiter `"Quellcode herunterladen"`.
3. Extrahieren Sie das zip-Archiv in Ihre Arbeitsumgebung.
4. Importieren Sie den entstandenen Projektordner von Ihrer Arbeitsumgebung in Ihre IDE.

## Erste Schritte
Anleitung zum Starten der Software und grundlegende Nutzung.

Nachdem das Projekt in Ihrer IDE importiert ist, starten Sie die Software, indem Sie GUIapp.java unter `template-segelflug // src/main/java // GUI // GUIapp.java` ausführen.

## Verwendung
Ausführliche Nutzungshinweise inkl. CSV-Import, GUI-Bedienung, usw.

Die folgenden Bilder des GUI sind mit Pfeilen versehen, welche auf Elemente hinweisen, welche im Text zur Orientierung mit [Zahlen] referenziert sind.

![Alt text](<Erklärung GUI oberer Teil.png>)

Wählen Sie Ihre gewünschte CSV-Datei aus. Hierzu klicken Sie auf das Dropdown-Menu [03] unter dem Kompass [02], wählen "Dateipfad" aus und navigieren im daraufhin angezeigten Explorer zu Ihrer gewünschten Datei.

Sobald Ihre Datei geladen ist, wird der Flugpfad auf dem 3D-Panel [01] sichtbar. 

![Alt text](<Erklärung GUI unterer Teil.png>)

Standardmässig wird beim Laden einer CSV-Datei im Plot [04] die Energie des Flugzeugs gegen die Zeit angezeigt. Um andere Grössen zu plotten, wählen Sie diese im Information Panel [05] mit einem Klick auf das jeweilige Kästchen an. Um Ihre Auswahl zu bestätigen und die Grössen im Plot darzustellen, klicken Sie anschliessen den Button "load lines" [08].

Im Abschnitt "Polynom" wird das Polynom der geladenen Energiefunktion angezeigt [06].

Nun haben Sie mehrere Möglichkeiten:

- Mit den Slidern [7] neben dem Plotpanel [04] können Sie zu einem beliebigen Zeitpunkt der Flugaufnahme springen (horizontaler Slider) oder den Zeitbereich, welchen das Plotpanel anzeigt vergrössern oder verkleinern (vertikaler Slider).
- Mit dem Button "Play" [08] können Sie den Flug abspielen. Zuvor kann über das Dropdown-Menu rechte neben dem Button die Geschwindigkeit, mit welcher der Flug abgespielt wird, eingestellt werden.


## Entwicklerdokumentation
Die Software wurde rein im Rahmen eines Projekts 2 entwickelt, weshalb auf Anweisungen zum Kompilieren und Bereitstellen der Software, sowie Informationen zu etwaigen Kontributionen verzichtet wird.

## Unittests
Um die Unittests durchzuführen, befolgen Sie folgende Schritte:

#### 1. Die richtige CSV-Datei in Java auswählen. 
In `src/main/java/backEnd` befindet sich die Klasse DataFilter. Wählen Sie in der Zeile 19 die CSV-Datei aus, welche Sie testen wollen. Diese CSV-Datei muss jedoch in `src/main/resources/terrain` gespeichert sein, dass DataFilter auf diese zugreifen kann.

#### 2. Die richtige CSV-Datei in Python auswählen.
In `src/main/resources` finden Sie die Pythondateien, welche zur Berechnung der Referenzwerte für die Unit-Tests dienen. Diese wären Polynomfitting01.py, WindspeedCaalc.py und Ausreisserberechnung.py. Geben Sie in den Python-Scripts denselben CSV-Dateipfad an wie in Schritt 1 in Java. Anschliessend wählen Sie einen Bereich für das Script Polynomfitting01.py, bzw. eine auszulesende Zeile für das Script WindspeedCalc.py aus. Des Weiteren wird stark dazu geraten, für das Script Ausreissertest.py die dedizierte Testdatei Ausreisser_Testfile.csv zu verwenden und bei Bedarf Spalteneinträge der Testspalte Medianfilter manuell nach Wunsch abzuändern, um Ausreisser zu provozieren. Dies ist notwendig, da das resultierende Array im nächsten Schritt hart in Java gecoded werden muss.

#### 3. Die selben Werte aus Python in Java übertragen.
Übertragen Sie die von Python berechneten Werte in die Testmethoden von Java. Diese sind zu finden unter `src/main/test`.

In jeder Methode sind die Stellen, an welchen Werte von Python übernommen werden sollen entsprechen kommentiert. Fügen Sie die erhaltenen Werte dort ein.

Bei den Endwerten darf bei der Windgeschwindigkeit und der Windrichtiûng maximal eine abweichung von 5% erfolgen, dass die Tests als gültig gelten. 

Beim Least Square Error darf der berechnete Wert von Java nicht grösser Sein, als der von Python. Desto kleiner er ist, desto präziser wurde er berechnet. Deshalb muss zum bestehen der Tests jeweils die Berechnung von Java kleiner sein. 

## Nutzungshinweis

Dieses Projekt wurde im Rahmen des Pro2E an der FHNW erstellt. Es dient ausschließlich zu akademischen Zwecken und ist nicht für eine kommerzielle Nutzung bestimmt. Eine Weiterverwendung oder Modifikation des Codes sollte mit den ursprünglichen Autoren abgesprochen werden.