/* Flight Panel


 * 
 * Copyright (C) 2024,  Manuel Di Cerbo, Nexus-Computing GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
// 

// This is a test commit
package flightpanel;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class FlightPanelTest {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					start();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public static void start() throws Exception {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			// ignore
		}

		JFrame frame = new JFrame("Flight Panel Test");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		final MainFlightPanel panel = new MainFlightPanel();
		panel.setPreferredSize(new Dimension(800, 600));
		panel.setDoubleBuffered(true);
		frame.add(panel);
		panel.init();

		frame.setResizable(true);

		panel.setFocusable(true);
		panel.requestFocus();

		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);


	}

}
