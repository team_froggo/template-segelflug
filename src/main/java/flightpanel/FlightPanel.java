/* Flight Panel
 * 
 * Copyright (C) 2024,  Manuel Di Cerbo, Nexus-Computing GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package flightpanel;

import java.awt.Canvas;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.JPanel;

import org.apache.commons.math3.geometry.euclidean.twod.Line;
import org.apache.commons.math3.geometry.euclidean.twod.PolygonsSet;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.apache.commons.math3.geometry.partitioning.Region.Location;

import com.jme3.app.SimpleApplication;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Cylinder;
import com.jme3.scene.shape.Sphere;
import com.jme3.system.AppSettings;
import com.jme3.system.JmeCanvasContext;
import com.jme3.util.SkyFactory;

import flightpanel.map.Chunk;
import flightpanel.map.Coordinate;
import flightpanel.map.IFlightPanel;
import flightpanel.map.IFlightPanelCallback;
import flightpanel.map.MapLoader;
import flightpanel.map.TopoLoader.TopoPolygon;

/**
 * @author Manuel Di Cerbo
 *
 */
public class FlightPanel extends JPanel implements IFlightPanel {

	private static final long serialVersionUID = 1L;

	private MapLoader mapLoader;
	private List<Chunk> chunks = new ArrayList<Chunk>();
	private Material markerMaterial;

	private Geometry marker = new Geometry("Marker", new Cylinder(20, 20, 0.25f, 100f, true));
	private List<Geometry> markers = new ArrayList<Geometry>();
	private List<Coordinate> coordinates = new ArrayList<>();

	private class CanvasApp extends SimpleApplication {

		private Spatial plane;

		public void addLights() {
			AmbientLight al = new AmbientLight();
			al.setColor(ColorRGBA.White.mult(0.1f));
			rootNode.addLight(al);

//			PointLight lamp_light = new PointLight();
//			lamp_light.setColor(ColorRGBA.White);
//			lamp_light.setRadius(8000f);
//			lamp_light.setPosition(new Vector3f(0, 100f, 0));
//			rootNode.addLight(lamp_light);

			DirectionalLight sun = new DirectionalLight();
			sun.setColor(ColorRGBA.White.mult(0.8f));
			sun.setDirection(new Vector3f(0, -.5f, 0).normalizeLocal());
			rootNode.addLight(sun);
		}

		public void addMarkers() {
			// Box box = new Box(1, 1, 1);
			Box box2 = new Box(1, 1, 1);

//			plane = new Geometry("Plane", box);
//			plane.setLocalTranslation(new Vector3f(1, 500f / 8000f * 255f, 1));
			marker.setLocalTranslation(new Vector3f(0, 0, 0));
			Quaternion rotation = new Quaternion().fromAngleAxis(-FastMath.HALF_PI, Vector3f.UNIT_X);
			marker.setLocalRotation(rotation);

			Geometry origin = new Geometry("Origin", box2);

//			markerMaterial = new Material(assetManager, // Create new material and...
//					"Common/MatDefs/Light/Lighting.j3md"); // ... specify .j3md file to use (illuminated).
//			markerMaterial.setBoolean("UseMaterialColors", true); // Set some parameters, e.g. blue.
//			markerMaterial.setColor("Ambient", ColorRGBA.Red); // ... color of this object
//			markerMaterial.setColor("Diffuse", ColorRGBA.Red);
//			markerMaterial.setFloat("Shininess", 64f);

			markerMaterial = new Material(assetManager, // Create new material and...
					"Common/MatDefs/Misc/Unshaded.j3md"); // ... specify .j3md file to use (illuminated).
			markerMaterial.setColor("Color", ColorRGBA.Green); // Set the color of the glow

//			plane.setMaterial(planeMaterial);
			origin.setMaterial(markerMaterial);
			marker.setMaterial(markerMaterial);

			// rootNode.attachChild(plane); // put this node in the scene
			rootNode.attachChild(origin);
			rootNode.attachChild(marker);
		}

		private void addAirCraft() {

			plane = assetManager.loadModel("glider.obj");

			// Create white material
			Material material = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
			material.setColor("Diffuse", com.jme3.math.ColorRGBA.White);
			material.setColor("Ambient", com.jme3.math.ColorRGBA.White);

			plane.setMaterial(material);
			plane.setLocalTranslation(0, 3000f * 0.25f, 0);
			plane.setLocalScale(0.1f);
			rootNode.attachChild(plane);

		}

		@Override
		public void simpleInitApp() {
			System.out.println(settings);

			flyCam.setMoveSpeed(100);
			flyCam.setEnabled(true);
			flyCam.setDragToRotate(true);
			flyCam.setRotationSpeed(2f);
			flyCam.setZoomSpeed(2f);

			cam.setFrustumFar(50000f);
			cam.setLocation(new Vector3f(0, 3000 * 0.25f, 0));
			cam.lookAt(new Vector3f(1000f, 0f, -1000f), new Vector3f(0, 1, 0));

			rootNode.attachChild(
					SkyFactory.createSky(getAssetManager(), "Skysphere.jpg", SkyFactory.EnvMapType.EquirectMap));

			addMarkers();
			addLights();
			addAirCraft();

			inputManager.addMapping("Exit", new KeyTrigger(KeyInput.KEY_ESCAPE));
			inputManager.addListener(new ActionListener() {

				@Override
				public void onAction(String name, boolean isPressed, float tpf) {
					if (!isPressed) {
						return;
					}
				}
			}, "Exit");

		}

		private void lookAtAirCraft() {
			getCamera().setLocation(plane.getWorldTranslation().subtract(new Vector3f(25f, -25f, -25f)));
			getCamera().lookAt(plane.getWorldTranslation(), Vector3f.UNIT_Y);
			getCamera().update();
		}

		public void setPlaneRotation(float roll, float pitch, float yaw) {
			Quaternion rollRotation = new Quaternion().fromAngleAxis(-roll, Vector3f.UNIT_X);
			Quaternion pitchRotation = new Quaternion().fromAngleAxis(-yaw - (float) (Math.PI / 2), Vector3f.UNIT_Y);
			Quaternion yawRotation = new Quaternion().fromAngleAxis(pitch, Vector3f.UNIT_Z);

			// Combine the rotations to get the final rotation quaternion
			Quaternion rotation = yawRotation.mult(pitchRotation).mult(rollRotation);
			app.plane.setLocalRotation(rotation);
		}
	}

	private CanvasApp app = new CanvasApp();

	private Canvas canvas;

	public FlightPanel(IFlightPanelCallback callback) {
		mapLoader = new MapLoader(pct -> {
			if (callback != null) {
				callback.onProgress(pct);
			}
		});
	}

	public void init() {
		setLayout(new GridBagLayout());

		AppSettings settings = new AppSettings(true);
		settings.setWidth(640);
		settings.setHeight(480);
		settings.setAudioRenderer(null);
		settings.setGammaCorrection(false);

		app.setSettings(settings);
		app.setDisplayStatView(false);
		app.setDisplayFps(false);
		app.createCanvas();
		app.setPauseOnLostFocus(false);

		JmeCanvasContext context = (JmeCanvasContext) app.getContext();
		canvas = context.getCanvas();
		app.initialize();
		app.startCanvas(true);
		add(canvas, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 0));

	}

	@Override
	public void loadCoordinates(double lat0, double lon0, double lat1, double lon1) {
		try {
			List<Chunk> chunks = mapLoader.loadChunks(lat0, lon0, lat1, lon1, app.getAssetManager());
			for (Chunk c : new ArrayList<Chunk>(this.chunks)) {
				app.getRootNode().detachChild(c.getTerrain());
				this.chunks.remove(c);
			}

			for (Chunk c : chunks) {
				this.chunks.add(c);
				app.getRootNode().attachChild(c.getTerrain());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void setAircraftPath(List<Coordinate> coordinates) {
		this.coordinates.clear();
		this.coordinates.addAll(coordinates);

		app.enqueue(() -> {
			float markerSize = 0.07f;
			Material checkpointMaterial = markerMaterial.clone();
			checkpointMaterial.setColor("Color", ColorRGBA.Green); // ... color of this object
//			checkpointMaterial.setColor("Diffuse", ColorRGBA.Green);

			Geometry startMarker = new Geometry("FlightMarker", new Sphere(20, 20, markerSize));
			startMarker.setMaterial(checkpointMaterial);
			setMarker(coordinates.get(0).lat, coordinates.get(0).lon, startMarker);
			setMarker(coordinates.get(0).lat, coordinates.get(0).lon, app.plane);

			setPlaneRotation(coordinates.get(0).roll, coordinates.get(0).pitch, coordinates.get(0).yaw);

			app.lookAtAirCraft();

			System.out.printf("%f, %f, %f\n", coordinates.get(0).roll, coordinates.get(0).pitch,
					coordinates.get(0).yaw);

			markers = coordinates.stream().skip(1).map(coordinate -> {
				Geometry m = new Geometry("FlightMarker", new Sphere(20, 20, markerSize));
				m.setMaterial(checkpointMaterial);

				setMarker(coordinate.lat, coordinate.lon, m);

				m.setLocalTranslation(m.getLocalTranslation().x,
						(float) (startMarker.getLocalTranslation().y + coordinate.ralt / 4f),
						m.getLocalTranslation().z);
				return m;
			}).collect(Collectors.toCollection(ArrayList::new));

			for (;;) {
				if (app.getRootNode().detachChildNamed("FlightMarker") == -1) {
					break;
				}
			}

			markers.add(0, startMarker);
			for (Geometry m : markers) {
				app.getRootNode().attachChild(m);

				Vector3f start = m.getLocalTranslation().clone();
				Vector3f end = m.getLocalTranslation().clone();
				end.setY(0);

				com.jme3.scene.shape.Line line = new com.jme3.scene.shape.Line(start, end);
				// Create a geometry for the line
				Geometry lineGeom = new Geometry("FlightMarker", line);
				lineGeom.setMaterial(checkpointMaterial);
				app.getRootNode().attachChild(lineGeom);
			}
		});

	}

	@Override
	public void setAircraftPosition(int index) {
		app.enqueue(() -> {
			try {
				Coordinate coord = this.coordinates.get(index);
				Geometry marker = this.markers.get(index);

				if (coord == null || marker == null) {
					return;
				}

				app.plane.setLocalTranslation(marker.getLocalTranslation());
				setPlaneRotation(coord.roll, coord.pitch, coord.yaw);
				// System.out.printf("%f, %f, %f\n", coord.roll, coord.pitch, coord.yaw);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

	private void setMarker(double lon, double lat, Spatial marker) {
		Chunk chunk = chunks.stream().filter(c -> {
			PolygonsSet ps = new PolygonsSet(1e-10,
					new Vector2D(c.getTopoResult().getPoly().geoPoints[0].lat,
							c.getTopoResult().getPoly().geoPoints[0].lon),
					new Vector2D(c.getTopoResult().getPoly().geoPoints[1].lat,
							c.getTopoResult().getPoly().geoPoints[1].lon),
					new Vector2D(c.getTopoResult().getPoly().geoPoints[2].lat,
							c.getTopoResult().getPoly().geoPoints[2].lon),
					new Vector2D(c.getTopoResult().getPoly().geoPoints[3].lat,
							c.getTopoResult().getPoly().geoPoints[3].lon));

			return ps.checkPoint(new Vector2D(lat, lon)) == Location.INSIDE;
		}).findFirst().orElse(null);

		if (chunk == null) {
			System.out.println("no matching chunk found");
			return;
		}

		final float f = 2000f / 2048f;

//		System.out.println("point");
//		System.out.println(chunk.getTopoResult().getPoly());

		double x = chunk.getTerrain().getLocalTranslation().x - 64f * f - 1.75f * f;
		double z = chunk.getTerrain().getLocalTranslation().z - 64f * f - 1.75f * f;

		TopoPolygon poly = chunk.getTopoResult().getPoly();
		Vector2D p1 = new Vector2D(poly.geoPoints[0].lat, poly.geoPoints[0].lon);
		Vector2D p2 = new Vector2D(poly.geoPoints[1].lat, poly.geoPoints[1].lon);
		Vector2D p4 = new Vector2D(poly.geoPoints[3].lat, poly.geoPoints[3].lon);

		Line l1 = new Line(p1, p2, 1e-10);
		Line l2 = new Line(p1, p4, 1e-10);
		Vector2D point = new Vector2D(lat, lon);

		Vector2D c1 = l1.toSpace(l1.toSubSpace(point));
		Vector2D c2 = l2.toSpace(l2.toSubSpace(point));

		// 47.478165, 8.311530
		double rdx = (c1.getX() - p1.getX()) / (p2.getX() - p1.getX());
		double rdy = 1. - (c2.getY() - p1.getY()) / (p4.getY() - p1.getY());

		double dx = rdx * 128f * f;
		double dz = rdy * 128f * f;

		float h = chunk.getTerrain().getHeight(new Vector2f((float) (x + dx), (float) (z + dz)));
//		System.out.println("h: in " + h);

//		dx = 128f * f;
//		dz = 128f * f;

		marker.setLocalTranslation((float) (x + dx), h, (float) (z + dz));
	}

	@Override
	public void setMarker(double lon, double lat) {
		setMarker(lon, lat, this.marker);
	}

	public void setPlaneRotation(double roll, double pitch, double yaw) {

		app.enqueue(() -> {
			app.setPlaneRotation((float) (roll / 180. * Math.PI), (float) (pitch / 180. * Math.PI),
					(float) (yaw / 180. * Math.PI));
		});
	}

}
