package flightpanel.misc;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

import com.jme3.app.SimpleApplication;
import com.jme3.material.Material;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Box;
import com.jme3.system.AppSettings;
import com.jme3.system.JmeCanvasContext;

public class TestSafeCanvas extends SimpleApplication {

	public static void main(String[] args) throws InterruptedException {
		AppSettings settings = new AppSettings(true);
		settings.setWidth(640);
		settings.setHeight(480);
		settings.setFrequency(-1);
		settings.setUseJoysticks(false);
		settings.setRenderer(AppSettings.LWJGL_OPENGL33);

		final TestSafeCanvas app = new TestSafeCanvas();
		app.setPauseOnLostFocus(false);
		app.setSettings(settings);
		app.createCanvas();
		app.startCanvas();

		JmeCanvasContext context = (JmeCanvasContext) app.getContext();
		Canvas canvas = context.getCanvas();
		canvas.setSize(settings.getWidth(), settings.getHeight());

		JFrame frame = new JFrame("Test");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				app.stop();
			}
		});

		frame.setMinimumSize(new Dimension(800, 600));
		frame.setPreferredSize(new Dimension(800, 600));

		frame.setLocationRelativeTo(null);
		frame.getContentPane().add(canvas);
		frame.pack();
		frame.setVisible(true);
	}

	@Override
	public void simpleInitApp() {
		flyCam.setDragToRotate(true);

		Box b = new Box(1, 1, 1);
		Geometry geom = new Geometry("Box", b);
		Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		mat.setTexture("ColorMap", assetManager.loadTexture("Interface/Logo/Monkey.jpg"));
		geom.setMaterial(mat);
		rootNode.attachChild(geom);
	}
}