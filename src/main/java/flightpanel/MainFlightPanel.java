/* Flight Panel
 * 
 * Copyright (C) 2024,  Manuel Di Cerbo, Nexus-Computing GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package flightpanel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ChangeListener;

import flightpanel.map.Coordinate;

public class MainFlightPanel extends JPanel {

	class MyListener implements ActionListener {

		public void actionPerformed(ActionEvent ev) {
			System.out.println("clicked");
		}
	}

	MyListener listener = new MyListener();

	private JButton btLoad = new JButton("Load Coordinates");
	private JButton btLoadCSV = new JButton("Load CSV");

//	private JTextField tfLon = new JTextField("8.312101");
//	private JTextField tfLat = new JTextField("47.473531");

	private JTextField tfLon = new JTextField("8.311530");
	private JTextField tfLat = new JTextField("47.478165");
	private JProgressBar jpLoading = new JProgressBar();

	private JSlider sliderRoll = new JSlider(0, 360);
	private JSlider sliderPitch = new JSlider(0, 360);
	private JSlider sliderYaw = new JSlider(0, 360);
	private JSlider sliderProgress = new JSlider(0, 1);

	private FlightPanel flightPanel;

	public void init() {
		setLayout(new GridBagLayout());

		border(tfLon, "Longitude");
		border(tfLat, "Latitude");
		border(jpLoading, "Progress Loading");

		jpLoading.setValue(0);

		flightPanel = new FlightPanel(pct -> {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					System.out.println("setting value: " + pct);
					jpLoading.setValue((int) pct + 1);
				}
			});

		});

		add(flightPanel, new GridBagConstraints(0, 0, 4, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(5, 5, 5, 5), 2, 2));

		add(jpLoading, new GridBagConstraints(0, 1, 4, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(5, 5, 5, 5), 2, 2));

		add(tfLat, new GridBagConstraints(0, 2, 1, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(5, 5, 5, 5), 2, 2));

		add(tfLon, new GridBagConstraints(1, 2, 1, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(5, 5, 5, 5), 2, 2));

		add(btLoad, new GridBagConstraints(2, 2, 1, 1, 0, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE,
				new Insets(5, 5, 5, 5), 2, 2));

		add(btLoadCSV, new GridBagConstraints(3, 2, 1, 1, 0, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE,
				new Insets(5, 5, 5, 5), 2, 2));

//		add(sliderRoll, new GridBagConstraints(0, 3, 4, 1, 1, 0, GridBagConstraints.CENTER,
//				GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
//
//		add(sliderPitch, new GridBagConstraints(0, 4, 4, 1, 1, 0, GridBagConstraints.CENTER,
//				GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
//
//		add(sliderYaw, new GridBagConstraints(0, 5, 4, 1, 1, 0, GridBagConstraints.CENTER,
//				GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));

		add(sliderProgress, new GridBagConstraints(0, 6, 4, 1, 1, 0, GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));

		flightPanel.init();

		btLoad.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				double lat0;
				double lon0;

				try {
					lat0 = Double.parseDouble(tfLat.getText());
					lon0 = Double.parseDouble(tfLon.getText());
					jpLoading.setValue(0);
					new Thread(new Runnable() {

						@Override
						public void run() {
							flightPanel.loadCoordinates(lat0 - 0.03, lon0 - 0.03, lat0 + 0.03, lon0 + 0.03);
							flightPanel.setMarker(lat0, lon0);
						}
					}).start();

				} catch (NumberFormatException e1) {
					e1.printStackTrace();
				}
			}

		});

		btLoadCSV.addActionListener(ev -> {
			new Thread(() -> {

				Path path;

				final int LAT = 8, LON = 7, RALT = 9, ROLL = 10, PITCH = 11, YAW = 12;

				try {

					path = Paths.get(ClassLoader.getSystemResource("data.csv").toURI());
					List<String> lines = Files.readAllLines(path);

					List<Coordinate> coords = lines.stream().skip(1).map(line -> {
						String[] parts = line.split(";");
						double lat = Double.parseDouble(parts[LAT]);
						double lon = Double.parseDouble(parts[LON]);
						double ralt = Double.parseDouble(parts[RALT]);
						double roll = Double.parseDouble(parts[ROLL]);
						double pitch = Double.parseDouble(parts[PITCH]);
						double yaw = Double.parseDouble(parts[YAW]);

						return new Coordinate(lat, lon, ralt, roll, pitch, yaw);
					}).toList();

					final double maxLat = coords.stream().max(Comparator.comparingDouble(c -> c.lat)).get().lat;
					final double minLat = coords.stream().min(Comparator.comparingDouble(c -> c.lat)).get().lat;
					final double maxLon = coords.stream().max(Comparator.comparingDouble(c -> c.lon)).get().lon;
					final double minLon = coords.stream().min(Comparator.comparingDouble(c -> c.lon)).get().lon;

					SwingUtilities.invokeLater(() -> {
						sliderProgress.setMaximum(coords.size() - 1);
						flightPanel.loadCoordinates(minLat, minLon, maxLat, maxLon);
						flightPanel.setAircraftPath(coords);
					});

				} catch (Exception e) {
					e.printStackTrace();
				}
			}).start();
		});

		ChangeListener changeListener = ev -> {
			flightPanel.setPlaneRotation(sliderRoll.getValue(), sliderPitch.getValue(), sliderYaw.getValue());
		};

		sliderRoll.addChangeListener(changeListener);
		sliderPitch.addChangeListener(changeListener);
		sliderYaw.addChangeListener(changeListener);

		sliderProgress.addChangeListener(ev -> {
			flightPanel.setAircraftPosition(sliderProgress.getValue());
		});

		// flightPanel.addFlightPath(path);

	}

	public static void border(JComponent comp, String title) {
		Border loweredetched = BorderFactory.createEtchedBorder(EtchedBorder.RAISED);
		Border titled = BorderFactory.createTitledBorder(loweredetched, title);
		comp.setBorder(titled);
	}
}
