/* Flight Panel
 * 
 * Copyright (C) 2024,  Manuel Di Cerbo, Nexus-Computing GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package flightpanel.map;

import java.util.Objects;

import com.jme3.terrain.geomipmap.TerrainQuad;

import flightpanel.map.TopoLoader.BoundryBox;
import flightpanel.map.TopoLoader.TopoResult;

public class Chunk {

	private final TopoResult topoResult;
	private final TerrainQuad terrain;

	public Chunk(TopoResult topoResult, TerrainQuad terrain) {
		this.topoResult = topoResult;
		this.terrain = terrain;
	}

	public BoundryBox getBoundry() {
		return topoResult.getBoundry();
	}

	public TerrainQuad getTerrain() {
		return terrain;
	}
	
	

	public TopoResult getTopoResult() {
		return topoResult;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Chunk)) {
			return super.equals(obj);
		}

		return ((Chunk) obj).getBoundry().equals(this.getBoundry());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getBoundry());
	}

}
