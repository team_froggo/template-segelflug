/* Flight Panel
 * 
 * Copyright (C) 2024,  Manuel Di Cerbo, Nexus-Computing GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package flightpanel.map;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.zip.ZipFile;

import javax.imageio.ImageIO;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.math.Vector2f;
import com.jme3.terrain.geomipmap.TerrainQuad;
import com.jme3.terrain.heightmap.AbstractHeightMap;
import com.jme3.texture.Texture;
import com.jme3.texture.Texture.WrapMode;
import com.jme3.texture.Texture2D;
import com.jme3.texture.plugins.AWTLoader;

import flightpanel.map.TopoLoader.TopoResult;

public class ChunkLoader {

	public Chunk load(TopoResult topoResult, Path image, Path altitude, AssetManager assetManager) throws Exception {

		String altitudeString;

		try (final ZipFile zipFile = new ZipFile(altitude.toFile())) {
			altitudeString = new String(zipFile.getInputStream(zipFile.stream().findFirst().get()).readAllBytes(),
					Charset.forName("utf-8"));
		} catch (Exception e) {
			Files.delete(altitude);
			throw new Exception("error loading zip");
		}

		String[] parts = altitudeString.split("\n");
		int num = parts.length - 1;
		final float[] heightmap = new float[num];

		for (int i = 1; i < parts.length; i++) {
			String line = parts[i];
			String[] values = line.split(" ");
//			double x = Double.valueOf(values[0]);
//			double y = Double.valueOf(values[1]);
			double z = Double.valueOf(values[2]);
			heightmap[i - 1] = (float) (z / 1000. * 512.);
		}

		final int sz = num - 1;

		AbstractHeightMap hm = new AbstractHeightMap() {

			public boolean load() {
				this.size = (int) Math.sqrt(sz);
				this.heightData = heightmap;

				System.out.println("size: " + this.size);
				return true;
			}
		};

		hm.smooth(128f);
		hm.load();
		int patchSize = 65;
		int totalSize = (int) Math.pow(2, (int) (Math.log(hm.getSize()) / Math.log(2) + 1));
		System.out.println("total size: " + totalSize);

		TerrainQuad terrain = new TerrainQuad(altitude.toString(), patchSize, totalSize + 1, hm.getHeightMap());

		Texture map = new Texture2D(new AWTLoader().load(ImageIO.read(image.toFile()), true));

		map.setWrap(WrapMode.EdgeClamp);

		Material mat = new Material(assetManager, "terrain/Terrain.j3md");
		mat.setTexture("Tex1", map);
		mat.setFloat("Tex1Scale", 2048f / 2000f);
		mat.setVector2("Offset", new Vector2f(0f, 0.024f));

		terrain.setMaterial(mat);
		terrain.setLocalTranslation(0, 0, 0);
		terrain.setLocalScale(0.25f, 0.25f, 0.25f);

		System.out.println("File: " + image.toFile().getName());

		return new Chunk(topoResult, terrain);
	}

}
