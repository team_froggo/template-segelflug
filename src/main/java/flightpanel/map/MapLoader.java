/* Flight Panel
 * 
 * Copyright (C) 2024,  Manuel Di Cerbo, Nexus-Computing GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package flightpanel.map;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Map.Entry;

import com.jme3.asset.AssetManager;

import flightpanel.map.TopoLoader.BoundryBox;
import flightpanel.map.TopoLoader.ProgressCallback;
import flightpanel.map.TopoLoader.TopoResult;

public class MapLoader {

	private final ProgressCallback progressCallback;

	public MapLoader(ProgressCallback progressCallback) {
		this.progressCallback = progressCallback;
	}

	public List<Chunk> loadChunks(double lat0, double lon0, double lat1, double lon1, AssetManager assetManager)
			throws Exception {
		ArrayList<Chunk> chunks = new ArrayList<>();

		TopoLoader imageLoader = new TopoLoader(
				"https://data.geo.admin.ch/api/stac/v0.9/collections/ch.swisstopo.swissimage-dop10",
				Path.of("cache", "images"), obj -> obj.optDouble("eo:gsd", 0) == 2);

		List<TopoResult> images = imageLoader.load(lat0, lon0, lat1, lon1, pct -> {
			progressCallback.onProgress(pct * 0.25);
		});

		TopoLoader altiLoader = new TopoLoader(
				"https://data.geo.admin.ch/api/stac/v0.9/collections/ch.swisstopo.swissalti3d",
				Path.of("cache", "altitude"), obj -> obj.optDouble("eo:gsd", 0) == 2
						&& obj.optString("type", "").equals("application/x.ascii-xyz+zip"));

		List<TopoResult> altitudes = altiLoader.load(lat0, lon0, lat1, lon1, pct -> {
			progressCallback.onProgress(pct * 0.25 + 25);
		});

		System.out.println("size: " + altitudes.size());

		ChunkLoader chunkLoader = new ChunkLoader();

		int count = 0;

		for (TopoResult altitude : altitudes) {
			Optional<TopoResult> img = images.stream().filter(i -> i.getBoundry().equals(altitude.getBoundry()))
					.findFirst();

			if (img == null) {
				System.out.println("no matching image found");
				continue;
			}

			try {
				final Chunk chnk = chunkLoader.load(altitude, img.get().getResourcePath(), altitude.getResourcePath(),
						assetManager);
				chunks.add(chnk);

				double pct = (++count / (double) altitudes.size() * 0.5 + 0.5) * 100f;
				if (progressCallback != null) {
					progressCallback.onProgress(pct);
				}

			} catch (Exception e) {
				e.printStackTrace();
				continue;
			}
		}

		final float f = 2000f / 2048f;

		int minX = chunks.stream().mapToInt(chnk -> chnk.getBoundry().getX()).min().orElse(0);
		int minY = chunks.stream().mapToInt(chnk -> chnk.getBoundry().getY()).min().orElse(0);

		for (Chunk chnk : chunks) {
			int x = (chnk.getBoundry().getX() - minX) * 128;
			int y = (chnk.getBoundry().getY() - minY) * 128;
			chnk.getTerrain().move(x * f, 0, -y * f);
		}

		return chunks;
	}

}
