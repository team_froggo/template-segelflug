/* Flight Panel
 * 
 * Copyright (C) 2024,  Manuel Di Cerbo, Nexus-Computing GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package flightpanel.map;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONObject;

import com.jme3.app.SimpleApplication;
import com.jme3.asset.AssetManager;

public class TopoLoader {

	public static class TopoResult {
		private final String href;
		private final BoundryBox boundry;
		private final TopoPolygon poly;
		private final Path resourcePath;

		public TopoResult(String href, BoundryBox boundry, TopoPolygon poly, Path resourcePath) {
			this.href = href;
			this.boundry = boundry;
			this.poly = poly;
			this.resourcePath = resourcePath;
		}

		public String getHref() {
			return href;
		}

		public BoundryBox getBoundry() {
			return boundry;
		}

		public TopoPolygon getPoly() {
			return poly;
		}

		public Path getResourcePath() {
			return resourcePath;
		}

	}

	public static final class GeoPoint {
		public final double lat, lon;

		public GeoPoint(double lat, double lon) {
			this.lat = lat;
			this.lon = lon;
		}
	}

	public static final class TopoPolygon {

		public final GeoPoint[] geoPoints = new GeoPoint[4];

		public TopoPolygon(GeoPoint p1, GeoPoint p2, GeoPoint p3, GeoPoint p4) {
			geoPoints[0] = p1;
			geoPoints[1] = p2;
			geoPoints[2] = p3;
			geoPoints[3] = p4;
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			for (GeoPoint p : geoPoints) {
				sb.append(String.format("(%.5f, %.5f)\n", p.lat, p.lon));
			}
			return sb.toString();
		}
	}

	private TopoPolygon polyFromJson(JSONArray coordinates) {
		JSONArray coords = coordinates.getJSONArray(0);

		GeoPoint[] points = new GeoPoint[4];
		for (int i = 0; i < 4; i++) {
			JSONArray latlon = coords.getJSONArray(i);
			points[i] = new GeoPoint(latlon.getDouble(0), latlon.getDouble(1));
		}

		return new TopoPolygon(points[0], points[1], points[2], points[3]);
	}

	public static final class BoundryBox {
		public final double lat0, lon0, lat1, lon1;

		private int x, y;

		public BoundryBox(double lat0, double lon0, double lat1, double lon1) {
			this.lat0 = lat0;
			this.lon0 = lon0;
			this.lat1 = lat1;
			this.lon1 = lon1;
		}

		@Override
		public String toString() {
			return String.format("%f, %f ====== %f, %f", lat0, lon0, lat1, lon1);
		}

		public int getX() {
			return x;
		}

		public void setX(int x) {
			this.x = x;
		}

		public int getY() {
			return y;
		}

		public void setY(int y) {
			this.y = y;
		}

		@Override
		public boolean equals(Object obj) {
			if (!(obj instanceof BoundryBox)) {
				return super.equals(obj);
			}

			final BoundryBox bb = (BoundryBox) obj;

			return bb.lat0 == lat0 && bb.lon0 == lon0 && bb.lat1 == lat1 && bb.lon1 == lon1;
		}

		@Override
		public int hashCode() {
			return Objects.hash(lat0, lon0, lat1, lon1);
		}
	}

	public interface ISelector {
		boolean select(JSONObject obj);
	}

	public interface ProgressCallback {
		void onProgress(double pct);
	}

	private final String baseUrl;
	private final Path cachePath;
	private final ISelector selector;

	public TopoLoader(String baseUrl, Path cachePath, ISelector selector) throws Exception {
		this.baseUrl = baseUrl;
		this.cachePath = cachePath;
		this.selector = selector;

		initDirs();
	}

	public void initDirs() throws Exception {
		if (Files.isDirectory(cachePath)) {
			return;
		}
		Files.createDirectories(cachePath);
	}

	public ArrayList<TopoLoader.TopoResult> load(double lat0, double lon0, double lat1, double lon1) throws Exception {
		return load(lat0, lon0, lat1, lon1, null);
	}

	public ArrayList<TopoLoader.TopoResult> load(double lat0, double lon0, double lat1, double lon1,
			ProgressCallback progressCallback) throws Exception {
		String url = String.format(Locale.US, "%s/items?bbox=%f,%f,%f,%f", baseUrl, lon0, lat0, lon1, lat1);

//		HashMap<BoundryBox, String> assetMap = new HashMap<>();
//		HashMap<TopoPolygon, String> polyMap = new HashMap<>();
//		HashMap<BoundryBox, Path> binaryMap = new HashMap<>();

		ArrayList<TopoResult> result = new ArrayList<TopoLoader.TopoResult>();

		HttpURLConnection conn = (HttpURLConnection) new URI(url).toURL().openConnection();

		String content = new String(conn.getInputStream().readAllBytes(), Charset.forName("utf-8"));
		JSONObject obj = new JSONObject(content);

		JSONArray features = obj.getJSONArray("features");
		for (int i = 0; i < features.length(); i++) {
			JSONObject item = features.getJSONObject(i);
			JSONArray bbox = item.getJSONArray("bbox");
			JSONArray coordinates = item.getJSONObject("geometry").getJSONArray("coordinates");

			BoundryBox boundry = new BoundryBox(bbox.getDouble(0), bbox.getDouble(1), bbox.getDouble(2),
					bbox.getDouble(3));

			TopoPolygon poly = polyFromJson(coordinates);
			System.out.println(poly);

			if (result.stream().anyMatch(r -> r.boundry.equals(boundry))) {
				System.out.println("duplicate");
				continue;
			}

			JSONObject assets = item.getJSONObject("assets");
			for (String k : assets.keySet()) {
				JSONObject asset = assets.getJSONObject(k);
				if (selector != null) {
					if (selector.select(asset)) {
						String href = asset.getString("href");
						Path resourcePath = cachePath.resolve(fileNameFromUrl(href));
						result.add(new TopoResult(href, boundry, poly, resourcePath));
						break;
					}
				}
			}
		}

//		List<BoundryBox> boundries = assetMap.keySet().stream().sorted(
//				Comparator.comparingDouble(v -> ((BoundryBox) v).lat0).thenComparingDouble(v -> ((BoundryBox) v).lon0))
//				.collect(Collectors.toList());

		// boundries.stream().forEach(b -> System.out.println(b));

		final ExecutorService executor = Executors.newFixedThreadPool(4);

		final AtomicInteger count = new AtomicInteger(0);

		for (final TopoResult r : result) {
			if (Files.exists(r.resourcePath)) {
				synchronized (this) {
					populateXY(r.boundry, r.resourcePath);

					if (progressCallback != null) {
						double pct = count.incrementAndGet() / (double) result.size() * 100.0;
						progressCallback.onProgress(pct);
					}
				}
			} else {
				executor.submit(new Runnable() {

					@Override
					public void run() {
						try {
							HttpURLConnection conn = (HttpURLConnection) new URI(r.href).toURL().openConnection();
							Files.copy(conn.getInputStream(), r.resourcePath, StandardCopyOption.REPLACE_EXISTING);
							synchronized (TopoLoader.this) {
								populateXY(r.boundry, r.resourcePath);

								if (progressCallback != null) {
									double pct = count.incrementAndGet() / (double) result.size() * 100.0;
									progressCallback.onProgress(pct);
								}
							}
						} catch (IOException e) {
							e.printStackTrace();
						} catch (URISyntaxException e) {
							e.printStackTrace();
						}

					}
				});
			}
		}

		executor.shutdown();

		try {
			executor.awaitTermination(5, TimeUnit.MINUTES);
		} catch (InterruptedException e) {
			executor.shutdownNow();
			e.printStackTrace();
		}

		return result;
	}

	private String fileNameFromUrl(String url) {
		String[] parts = url.split("/");
		return parts[parts.length - 1];
	}

	private void populateXY(BoundryBox bb, Path path) {
		String filename = path.toFile().getName();
		System.out.println(filename);
		Matcher m = Pattern.compile("(.*_)(\\d+)-(\\d+)_(.*)").matcher(filename);
		m.find();
		bb.setX(Integer.valueOf(m.group(2)));
		bb.setY(Integer.valueOf(m.group(3)));
	}

	public static void main(String[] args) throws Exception {

		// baden 8.340861, 47.455872,8.293676, 47.477450
		TopoLoader imageLoader = new TopoLoader(
				"https://data.geo.admin.ch/api/stac/v0.9/collections/ch.swisstopo.swissimage-dop10",
				Path.of("cache", "images"), obj -> obj.optDouble("eo:gsd", 0) == 2);

		ArrayList<TopoResult> images = imageLoader.load(46.442604, 7.915954, 46.350885, 8.305508);

		TopoLoader altiLoader = new TopoLoader(
				"https://data.geo.admin.ch/api/stac/v0.9/collections/ch.swisstopo.swissalti3d",
				Path.of("cache", "altitude"), obj -> obj.optDouble("eo:gsd", 0) == 2
						&& obj.optString("type", "").equals("application/x.ascii-xyz+zip"));

		ArrayList<TopoResult> altitudes = altiLoader.load(46.442604, 7.915954, 46.350885, 8.305508);

		System.out.println("size: " + altitudes.size());

		ChunkLoader chunkLoader = new ChunkLoader();

		final ArrayBlockingQueue<AssetManager> mgrQueue = new ArrayBlockingQueue<>(1);
		SimpleApplication app = new SimpleApplication() {

			@Override
			public void simpleInitApp() {
				synchronized (this) {
					flyCam.setMoveSpeed(500);
					flyCam.setEnabled(true);
					cam.setFrustumFar(50000f);

					mgrQueue.offer(getAssetManager());
				}
			}
		};

		app.start();

		AssetManager mgr = mgrQueue.take();

		ArrayList<Chunk> chunks = new ArrayList<>();
		final AtomicInteger i = new AtomicInteger(0);

//		Optional<TopoResult> r = altitudes.stream().filter(a -> a.getBoundry.equals().findFirst();
//
//		if (r == null) {
//			System.out.println("no matching image found");
//			continue;
//		}
//
//		try {
//			final Chunk chnk = chunkLoader.load(r.get().boundry, r.get().resourcePath, altitude.getValue(), mgr);
//			chunks.add(chnk);
//		} catch (Exception e2) {
//			continue;
//		}
//		final float f = 2000f / 2048f;
//
//		app.enqueue(new Runnable() {
//			@Override
//			public void run() {
//
//				int minX = chunks.stream().mapToInt(chnk -> chnk.getBoundry().getX()).min().orElse(0);
//				int minY = chunks.stream().mapToInt(chnk -> chnk.getBoundry().getY()).min().orElse(0);
//
//				for (Chunk chnk : chunks) {
//					app.getRootNode().attachChild(chnk.getTerrain());
//
//					int x = (chnk.getBoundry().getX() - minX) * 128;
//					int y = (chnk.getBoundry().getY() - minY) * 128;
//					chnk.getTerrain().move(x * f, 0, -y * f);
//
//					i.incrementAndGet();
//				}

//				for (Chunk chnk : sorted) {
//					lodControl.addTerrain(chnk.getTerrain());
//				}
//				app.getRootNode().addControl(lodControl);
//			}
//		});

	}

}
