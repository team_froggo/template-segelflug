
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;

import javax.swing.JPanel;

import util.Utility;

public class AnimationPanel extends JPanel {
	private static final long serialVersionUID = 1L;


	public AnimationPanel() {
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		//make variable skywidth
		
		Graphics2D g2d = (Graphics2D) g;
	
		g2d.setColor(Color.WHITE);
		
		Image pika = Utility.loadResourceImage("Alps");
		g2d.drawImage(pika, 0,0,getWidth(),getHeight(), null);
	}
}
