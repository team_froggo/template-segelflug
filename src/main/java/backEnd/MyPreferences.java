package backEnd;

import org.supercsv.prefs.CsvPreference;

public class MyPreferences {
    public static final CsvPreference SEMICOLON_DELIMITED = new CsvPreference.Builder('"', ';', "\n").build();
}