package backEnd;
import java.nio.file.Path;
import java.util.Arrays;
import org.apache.commons.math3.analysis.polynomials.PolynomialFunction;
public class DataFilter {
	SuperCSVLoader loader;
	 double[][] loadedData;
public DataFilter(String filePath) {
		super();
		// TODO Auto-generated constructor stub
		this.loader = new SuperCSVLoader();
		this.loader.setFilepath(filePath);
		loadData();
	}
public DataFilter() {
	super();
	// TODO Auto-generated constructor stub
	this.loader = new SuperCSVLoader();
	this.loader.setFilepath("data.csv");
	loadData();
}
public  void changepath(String path) {
	this.loader.setFilepath(path);
	loadData();
}
public  void changepath(Path path) {
	this.loader.setFilepath(path);
	loadData();
}

public double[] getWindspeed() {
	WindCalculator wind = new WindCalculator(this);
	return wind.getWindstrength();
}
public double[] getwinddirection() {
	WindCalculator wind = new WindCalculator(this);
	return wind.getWinddirection();
}

public double[] filterArrayMedian(double[] source) {
	int iterations = source.length-4;
	double[] sortedarray = new double[source.length];
	sortedarray[0]=source[0];
	sortedarray[source.length-2]=source[source.length-2];
	sortedarray[1]=source[1];
	sortedarray[source.length-1]=source[source.length-1];
	int i = 0;
	while(i<iterations) {
		double[] window = Arrays.copyOfRange(source, i, i+5);
		Arrays.sort(window);
		
		double median = window[2];
		sortedarray[i+2]=median;
		i++;
	}
	return sortedarray;
}

public double[] getTest(){
	SuperCSVLoader loader = new SuperCSVLoader();
	loader.setFilepath("Ausreisser_Testfile.csv");
	String[] header = {"Testspalte Medianfilter"};
	double[] data;
	try {
		data = filterArrayMedian(loader.loadColumns(header)[0]);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		data=null;
	}

	return data;
}


public double[] getClimbrate() {
	Polynomfitter poly = new Polynomfitter(this);
	return poly.getClimbRate();
}
public String getPolynomString(int indexstart, int indexend,int grad) {

	Polynomfitter poly;
	try {
		poly = new Polynomfitter(this);
		return poly.getPolynomString(Math.max(0, indexstart), indexend, grad);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return "Please reduce polynom degree";
}
public double[] getPolynomData(int indexstart, int indexend, int degree) {
	Polynomfitter poly = new Polynomfitter(this);
	return poly.getPolynomxValues(Math.max(0, indexstart), indexend, degree);
}
public void loadData() {
this.loadedData = new double[23][];
	for( int i = 0; i<23;i++) {
		this.loadedData[i]=loadCSVColumn(i);
	}
}
private double[] loadCSVColumn(int index) {
	 double[] data = null;
	    String[] keysarray = {
	        "timestart", "timeweek", "humidity", "speedground", "speedair",
	        "temp", "pressure", "long", "lat", "height", "roll", "pitch",
	        "yaw", "groundcourse", "energie","Windspeed","Winddirection","Climbrate", "gyrx", "gyry", "gyrz",
	        "accex", "accey", "accez"
	    };
	    if( index == 15) {
	    	return getWindspeed();
	    }else if(index==16) {
	    	return getwinddirection();
	    }else if(index==17) {
	    	return getClimbrate();
	    }else if(index ==-1) {
	    	getTimeS();
	    }
	    try {
	        String[] key = { keysarray[index] };  // Potential ArrayIndexOutOfBoundsException
	        if (index > 2) {
	            data = filterArrayMedian(loader.loadColumns(key)[0]);
	        } else {
	            data = loader.loadColumns(key)[0];
	        }
	    } catch (ArrayIndexOutOfBoundsException e) {
	        // Handle the case where 'index' is out of bounds
	        System.err.println("Index out of bounds: " + index);
	        // Optionally, return a default value or rethrow a more specific, custom exception
	    } catch (Exception e) {
	        // Handle other exceptions that might occur
	        System.err.println("An error occurred: " + e.getMessage());
	        // Optionally, return a default value or rethrow a more specific, custom exception
	    }
	    return data;
}
public double[] getFilterData(int index) {
	if(index ==-1) {
	return getTimeS();
}
   return this.loadedData[index];
}
public double getpolynomError(int indexstart, int indexend,int grad) {
	Polynomfitter poly = new Polynomfitter(this);
	return poly.getError(Math.max(0, indexstart), indexend, grad);
}
public PolynomialFunction getpf(int indexstart, int indexend,int grad) {
	Polynomfitter poly = new Polynomfitter(this);
	return poly.getpolyFunction(Math.max(0, indexstart), indexend, grad,true);
}
public void writePolynomtoFile(Path path,int indexend, int indexstart, int grad) {
	PolyFileWriter pofw = new PolyFileWriter(path, this, indexend, Math.max(0, indexstart), grad);
	pofw.writetoFile();
}
public double[] getTimeS() {
	double[] timeuS = getFilterData(0);
	double [] timeS = new double[timeuS.length];
	for(int i = 0; i<timeuS.length;i++) {
		timeS[i]=timeuS[i]/1000000;
	}
	return timeS;
}

public double getaverageClimbrate(int indexstart, int indexend) {
	double[] heightdata = getFilterData(9);
	double[] timedata= getFilterData(0);
	double StartHeight = heightdata[Math.max(0, indexstart)];
	double EndHeight = heightdata[indexend];
	double averagerate = (EndHeight-StartHeight)/((timedata[indexend]-timedata[Math.max(0, indexstart)])/1000000);
	return averagerate;
}

}
