package backEnd;
import java.lang.Math;
public class WindCalculator {
	DataFilter data;
	
	public WindCalculator(DataFilter data) {
		super();
		// TODO Auto-generated constructor stub
		this.data = data;
	}

	public double[] getWinddirection() {
		return calculatepolar()[1];
	}
	public double[] getWindstrength() {
		return calculatepolar()[0];
	}
	public double[][] calculateXY(){
		double[][] rawdata = {data.getFilterData(3),data.getFilterData(13),data.getFilterData(4),data.getFilterData(12)};
		double[][] windarrayxy = new double[rawdata[0].length][2];
		for(int i = 0 ; i < rawdata[0].length; i++) {
			double groundx=rawdata[0][i]*Math.sin(Math.toRadians(rawdata[1][i]));
			double groundy=rawdata[0][i]*Math.cos(Math.toRadians(rawdata[1][i]));
			double airx=rawdata[2][i]*Math.sin(Math.toRadians(rawdata[3][i]));
			double airy=rawdata[2][i]*Math.cos(Math.toRadians(rawdata[3][i]));
			windarrayxy[i][0]=groundx - airx;
			windarrayxy[i][1]= groundy-airy;
		}
		return windarrayxy;
	}
	private double[][] calculatepolar(){
		double [][] arrayxy= calculateXY();
		double [][] arraypolar = new double[2][arrayxy.length];
		int i = 0;
		for(double[] item : arrayxy) {
			double mag = Math.sqrt(item[0]*item[0]+item[1]*item[1]);
			double angle = Math.atan2(item[1], item[0]);
			arraypolar[0][i]=mag;
			arraypolar[1][i]=(Math.toDegrees(angle)+360)%360;
			i++;
		}
		return arraypolar;
	}
}
