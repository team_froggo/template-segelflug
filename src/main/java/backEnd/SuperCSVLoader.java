package backEnd;

import java.io.FileReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

import org.supercsv.io.CsvListReader;

public class SuperCSVLoader {
	private String filepath;
	private Path path;
	Dictionary<String, String> dict = new Hashtable<>();

	public SuperCSVLoader() {
		super();
		dict.put("timestart", "Time since System Start [us]");
		dict.put("timeweek", "Week time [milliseconds since start of GPS week]");
		dict.put("humidity", "Humidity [%]");
		dict.put("speedground", "Groundspeed [m/s]");
		dict.put("speedair", "Airspeed [m/s]");
		dict.put("temp", "Temperature [deg C]");
		dict.put("pressure", "Airpressure [kPas]");
		dict.put("long", "Longitude");
		dict.put("lat", "Latitude");
		dict.put("height", "Flight Height [m]");
		dict.put("roll", "Roll [deg]");
		dict.put("pitch", "Pitch [deg]");
		dict.put("yaw", "Yaw [deg]");
		dict.put("groundcourse", "Ground course [deg]");
		dict.put("energie", "Energie [m]");
		dict.put("gyrx", "GyroX [rad/s]");
		dict.put("gyry", "GyroY [rad/s]");
		dict.put("gyrz", "GyroZ [rad/s]");
		dict.put("accex", "AcceX [m/s/s]");
		dict.put("accey", "AcceY [m/s/s]");
		dict.put("accez", "AcceZ [m/s/s]");
		
	}

	public void setFilepath(String path) {
		this.filepath = path;
		try {
		this.path = Paths.get(ClassLoader.getSystemResource(filepath).toURI());
		}catch(Exception e) {
			System.out.println("oops");
		}
	}
	public void setFilepath(Path path) {
		
		this.path = path;
		
	}

	public String simplehead(String head) {
		if (dict.get(head) == null) {
			return head;
		}
		return dict.get(head);
	}
	public void loadData(String[] headersToLoad) {
		try {
			getAlldata();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public double[][] loadColumns(String[] headersToLoad) throws Exception {
		List<List<String>> data = new ArrayList<>();

		try (CsvListReader listReader = new CsvListReader(new FileReader(path.toFile()),
				MyPreferences.SEMICOLON_DELIMITED)) {
			final String[] header = listReader.getHeader(true);
			List<Integer> columnIndexes = new ArrayList<>();
			for (String headerToLoad : headersToLoad) {
				int index = Arrays.asList(header).indexOf(simplehead(headerToLoad));
				if (index != -1) {
					columnIndexes.add(index);
				} else {
					System.out.println("Header not found: " + headerToLoad);
				}
			}

			List<String> row;
			while ((row = listReader.read()) != null) {
				List<String> selectedColumns = new ArrayList<>();
				for (Integer columnIndex : columnIndexes) {
					selectedColumns.add(row.get(columnIndex));
				}
				if (!selectedColumns.isEmpty()) {
					data.add(selectedColumns);
				}
			}
		}

		if (data.isEmpty()) {
			System.out.println("No data found or headers did not match.");
			return (double[][]) new Object[0]; // or return null depending on how you want to handle this case
		}

		if (headersToLoad.length == 1) {
			double[][] singleColumnArray = new double[1][data.size()];
			for (int i = 0; i < data.size(); i++) {
				singleColumnArray[0][i] = Double.parseDouble(data.get(i).get(0)); // Safe as data is checked to be
																					// non-empty
			}
			return singleColumnArray;
		} else {
			double[][] multiColumnArray = new double[headersToLoad.length][data.size()];
			for (int i = 0; i < data.size(); i++) {
				for (int j = 0; j < headersToLoad.length; j++) {
					multiColumnArray[j][i] = Double.parseDouble(data.get(i).get(j)); // Safe as data is checked to be
																						// non-empty
				}
			}
			return multiColumnArray;
		}
	}

	public double[][] getAlldata() throws Exception {
		String[] keysArray = { "timestart", "timeweek", "humidity", "speedground", "speedair", "temp", "pressure",
				"long", "lat", "height", "roll", "pitch", "yaw", "groundcourse", "energie", "gyrx", "gyry", "gyrz",
				"accex", "accey", "accez" };
		return loadColumns(keysArray);
	}

}
