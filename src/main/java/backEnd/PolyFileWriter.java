package backEnd;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.*;
import org.apache.commons.math3.analysis.polynomials.PolynomialFunction;
public class PolyFileWriter {
String[] writedata;
Path path;
double[] coeffs;
DataFilter data;
int indexend;
int indexstart;
int grad;


public void setPath(Path path) {
	this.path = path;
}

public void setIndexend(int indexend) {
	this.indexend = indexend;
}

public void setIndexstart(int indexstart) {
	this.indexstart = indexstart;
}

public void setGrad(int grad) {
	this.grad = grad;
}

public PolyFileWriter(Path path, DataFilter data, int indexend, int indexstart, int grad) {
	super();
	this.path = path;
	this.data = data;
	this.indexend = indexend;
	this.indexstart = indexstart;
	this.grad = grad;
}

private void writeDatatoFile() {
	try (BufferedWriter writer = Files.newBufferedWriter(path, StandardOpenOption.CREATE, StandardOpenOption.WRITE)) {
        for (String line : writedata) {
            writer.write(line);
            writer.newLine(); // Adds a new line after each string
        }
        System.out.println("Data written to file successfully.");
    } catch (IOException e) {
        System.err.println("An error occurred:");
        e.printStackTrace();
    }
}
private void makeData() {
	double [] coeffs = data.getpf(indexstart, indexend, grad).getCoefficients();
	System.out.println(coeffs[0]);
	writedata = new String[coeffs.length+2];
	int i = 0;
	for( double item : coeffs) {
		String numner = "";
		numner =String.format("%.2e",item);
    	numner = numner.replace("e+00", "");
    
    	numner = numner.replace("e-0", "e-");
    	numner = numner.replace("e+0", "e+");
		writedata[i]= "Koeffizinet " + (i)+ "ten Grades ist: "+numner;
		i++;
				}
	writedata[i]="";
	writedata[i+1]="Least square Error ist: " + data.getpolynomError(indexstart, indexend, grad);
}


public void writetoFile() {
	makeData();
	writeDatatoFile();
}
}