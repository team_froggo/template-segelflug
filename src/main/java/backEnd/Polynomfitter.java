package backEnd;

import org.apache.commons.math3.analysis.polynomials.PolynomialFunction;
import org.apache.commons.math3.fitting.PolynomialCurveFitter;
import org.apache.commons.math3.fitting.WeightedObservedPoints;
import java.util.Arrays;   
public class Polynomfitter {
	 private final String[] SUPERSCRIPTS = {"\u2070", "\u00B9", "\u00B2", "\u00B3", "\u2074", 
             "\u2075", "\u2076", "\u2077", "\u2078", "\u2079"};

	private double[] data;
	DataFilter filter ;
	public Polynomfitter(DataFilter Filter) {
		super();
		this.filter = Filter;
		String[] test = {"energie","timestart"}; 
		data = Filter.getFilterData(14);
		
		// TODO Auto-generated constructor stub
	}
	public Polynomfitter() {
		super();
		filter = new DataFilter();
		String[] test = {"energie","timestart"}; 
		data = filter.getFilterData(14);
		
		// TODO Auto-generated constructor stub
	}
	public void changeDataset(int dataindex) {
		this.data = this.filter.getFilterData(dataindex);
	}
	public double[] getClimbRate() {
		double[] dataold = this.data;
		changeDataset(9);
		double[] timedata = filter.getFilterData(0);
		double [] outarr = new double[getcurrentDatasize()];
		PolynomialFunction hpf = getpolyFunctionwholeData(40,true);
		PolynomialFunction abhpf = hpf.polynomialDerivative();
		
		for(int i =0; i<getcurrentDatasize();i++) {
			outarr[i]=abhpf.value(timedata[i]/1000000);
		}
		this.data=dataold;
		return outarr;
	}
	public String toSuperscript(int number) {
        StringBuilder result = new StringBuilder();
        String numberStr = Integer.toString(number);
        for (int i = 0; i < numberStr.length(); i++) {
            int digit = numberStr.charAt(i) - '0';
            result.append(SUPERSCRIPTS[digit]);
        }
        return result.toString();
	}
	
	public double[] getCoeff(int indexstart, int indexend,int grad,boolean truescale) {
		WeightedObservedPoints obs = new WeightedObservedPoints();
	    int i = indexstart;
	    double[] timedata = filter.getFilterData(0);
	    if(truescale) {
	    for(double item : Arrays.copyOfRange(data,indexstart,indexend) ){
	        obs.add(timedata[i]/1000000,  item);
	        i++;
	        if(i>indexend) {
	            break;
	        }
	    }
	    }else {
	    for(double item : Arrays.copyOfRange(data,indexstart,indexend) ){
	        obs.add(i,  item);
	        i++;
	        if(i>indexend) {
	            break;
	        }
	    }
	    }
	    final PolynomialCurveFitter fitter = PolynomialCurveFitter.create(grad);
	    return fitter.fit(obs.toList());
	}
	public String getPolynomString(int indexstart, int indexend,int grad) {
	   
	    double[] coeff = getCoeff(indexstart, indexend, grad,true);
	    StringBuilder polynomBuilder = new StringBuilder();
	    int j = coeff.length-1;
	    String numner = "";
	    coeff= arrayreverser(coeff);
	    for(double item : coeff) {

	    	numner =String.format("%.2e",item);
	    	numner = numner.replace("e+00", "");
	    
	    	numner = numner.replace("e-0", "e-");
	    	numner = numner.replace("e+0", "e+");
	    	if(item >=0) {
	    		polynomBuilder.append(numner).append(" x").append(toSuperscript(j)).append(" + "); 
	    	}else {
	    		String istring =numner.substring(1);
	    		try{
	    			polynomBuilder.replace(polynomBuilder.length()-2, polynomBuilder.length()-1, "-");
	    		}catch(Exception e){
	    			polynomBuilder.append("- ");
	    			
	    		}
	    		polynomBuilder.append(istring).append(" x").append(toSuperscript(j)).append(" + "); 
	    	}
	        j--;
	    }
	    String outsting = polynomBuilder.toString();

	    return outsting.substring(0, outsting.length() - 6); // Entferne die letzten sechs Zeichen ", "
	
	}
	public String getPolynomStringofIndex(int indexstart, int indexend,int grad,int index) {
	    double[] dataold=this.data;
	    changeDataset(index);
	    double[] coeff =  getCoeff(indexstart, indexend, grad,true);
	    StringBuilder polynomBuilder = new StringBuilder();
	    int j = coeff.length-1;
	    String numner = "";
	    coeff= arrayreverser(coeff);
	    for(double item : coeff) {

	    	numner =String.format("%.2e",item);
	    	numner = numner.replace("e+00", "");
	    
	    	numner = numner.replace("e-0", "e-");
	    	numner = numner.replace("e+0", "e+");
	    	if(item >=0) {
	    		polynomBuilder.append(numner).append(" x").append(toSuperscript(j)).append(" + "); 
	    	}else {
	    		String istring =numner.substring(1);
	    		try{
	    			polynomBuilder.replace(polynomBuilder.length()-2, polynomBuilder.length()-1, "-");
	    		}catch(Exception e){
	    			polynomBuilder.append("- ");
	    			
	    		}
	    		polynomBuilder.append(istring).append(" x").append(toSuperscript(j)).append(" + "); 
	    	}
	        j--;
	    }
	    String outsting = polynomBuilder.toString();
	    this.data=dataold;
	    return outsting.substring(0, outsting.length() - 6); // Entferne die letzten sechs Zeichen ", "
	}
	public String getPolynomStringofData(int indexstart, int indexend,int grad,double[] data) {
		double[] coeff = getCoeff(indexstart, indexend, grad,true);
	    StringBuilder polynomBuilder = new StringBuilder();
	    int j = coeff.length-1;
	    String numner = "";
	    coeff= arrayreverser(coeff);
	    for(double item : coeff) {
	    	numner =String.format("%.2e",item);
	    	numner = numner.replace("e+00", "");
	    	numner = numner.replace("e-0", "e-");
	    	numner = numner.replace("e+0", "e+");
	    	if(item >=0) {
	    		polynomBuilder.append(numner).append(" x").append(toSuperscript(j)).append(" + "); 
	    	}else {
	    		String istring = numner.substring(1);
	    		try{
	    			polynomBuilder.replace(polynomBuilder.length()-2, polynomBuilder.length()-1, "-");
	    		}catch(Exception e){
	    			polynomBuilder.append("- ");
	    			}
	    		polynomBuilder.append(istring).append(" x").append(toSuperscript(j)).append(" + "); 
	    	}
	        j--;
	    }
	    String outsting = polynomBuilder.toString();
	    return outsting.substring(0, outsting.length() - 6); // Entferne die letzten sechs Zeichen ", "
	}
	private int getcurrentDatasize() {
		return this.data.length;
	}
	public double getError(int indexstart, int indexend,int grad) {
		changeDataset(14);
	    PolynomialFunction pf = getpolyFunction(indexstart, indexend, grad,false);
	 


	    double leastSquaresError = 0;
	    for (int i = indexstart; i <= indexend; i++) {
            double y = data[i];
            double yPredicted = pf.value(i);
            double residual = y - yPredicted;
            leastSquaresError += residual * residual;
        }
       return leastSquaresError;

	}
	public PolynomialFunction getpolyFunction(int indexstart, int indexend, int degree,boolean truescale) {
		PolynomialFunction pf = new PolynomialFunction(getCoeff(indexstart,indexend,degree,truescale));
		return pf;
	}
	public PolynomialFunction getpolyFunctionwholeData(int degree,boolean truescale) {
		PolynomialFunction pf = new PolynomialFunction(getCoeff(0,this.data.length ,degree,truescale));
		return pf;
	}
	public double[] getPolynomxValues(int indexstart, int indexend, int degree) {
        
        int i = indexstart;
        
        PolynomialFunction pf = getpolyFunction(indexstart, indexend, degree,true);

        double[] polynomialValues = new double[indexend - indexstart + 1];
      
        for (int j = 0; j < polynomialValues.length; j++, i++) {
            polynomialValues[j] = pf.value(i);
        }
        return polynomialValues;
    }

	private double[] arrayreverser(double[] arr) {
		int i = arr.length-1;
		double[] outarr=new double[arr.length];
		for (double item : arr) {
			outarr[i]=item;
			i--;
		}
		return outarr;
	}
}