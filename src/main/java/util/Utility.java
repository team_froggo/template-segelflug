package util;

import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.JComponent;

public class Utility {
	public static Image loadResourceImage(String strBild) {
		try (final InputStream is = ClassLoader.getSystemResourceAsStream("res/" + strBild + ".png")) {
			return ImageIO.read(is);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Font loadFont() {
		Font font = null;
		InputStream is = ClassLoader.getSystemResourceAsStream("font/font.ttf");

		try {
			font = Font.createFont(Font.TRUETYPE_FONT, is).deriveFont(16f);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return font;
	}

	static class ImageSelection implements Transferable {
		private Image image;

		public ImageSelection(Image image) {
			this.image = image;
		}

		public DataFlavor[] getTransferDataFlavors() {
			return new DataFlavor[] { DataFlavor.imageFlavor };
		}

		public boolean isDataFlavorSupported(DataFlavor flavor) {
			return DataFlavor.imageFlavor.equals(flavor);
		}

		public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
			if (!DataFlavor.imageFlavor.equals(flavor)) {
				throw new UnsupportedFlavorException(flavor);
			}
			return image;
		}
	}

	public static void graphicsToClipBoard(JComponent component) {

		BufferedImage image = new BufferedImage(component.getWidth(), component.getHeight(),
				BufferedImage.TYPE_INT_RGB);
		component.paint(image.getGraphics()); // alternately use .printAll(..)
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new ImageSelection(image), null);

	}
}
