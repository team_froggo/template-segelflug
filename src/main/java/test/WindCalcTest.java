package test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import backEnd.DataFilter;
import backEnd.WindCalculator;

public class WindCalcTest {

    @Test
    public void testWindCalculations() throws Exception {
        // Erzeuge eine Instanz von DataFilter und WindCalculator
        DataFilter data = new DataFilter();
        WindCalculator wind = new WindCalculator(data);

        // Zeilennummer, die getestet werden soll
        int rowIndex = 245;

        // Berechne Windstärke und -richtung mit Java
        double[] windStrengthJava = wind.getWindstrength();
        double[] windDirectionJava = wind.getWinddirection();

        // Erwartete Windstärke und -richtung aus Python (diese Werte sollten aus Ihrer Python-Anwendung stammen)
        double expectedWindStrength = 0.871523;  // Beispielwert
        double expectedWindDirection = 72.222134;  // Beispielwert

        // 5% Abweichung zulassen
        double deltaStrength = 0.05 * expectedWindStrength;
        double deltaDirection = 0.05 * expectedWindDirection;

        // Vergleiche den tatsächlich berechneten Windstärke mit dem erwarteten Wert
        assertEquals(expectedWindStrength, windStrengthJava[rowIndex], deltaStrength);
        // Vergleiche den tatsächlich berechneten Windrichtung mit dem erwarteten Wert
        assertEquals(expectedWindDirection, windDirectionJava[rowIndex], deltaDirection);
    }
}
