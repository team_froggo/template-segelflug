package test;

import static org.junit.Assert.assertArrayEquals;

import java.io.File;
import java.io.FileReader;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.supercsv.io.CsvListReader;
import org.supercsv.prefs.CsvPreference;

import backEnd.DataFilter;

public class MedianTest {
	private double[] medianTestwerteArray;

	@Before
	public void setUp() throws Exception {
		// Pfad zu Testdatei.csv
		String csvFilePath = "Ausreisser_Testfile.csv";

		// Liste, um die Werte zu speichern
		List<Double> medianTestwerte = new ArrayList<>();

		File file = Paths.get(ClassLoader.getSystemResource(csvFilePath).toURI()).toFile();

		// Reader initialisieren
		try (CsvListReader listReader = new CsvListReader(new FileReader(file), CsvPreference.EXCEL_NORTH_EUROPE_PREFERENCE)) {
			List<String> line;
			int count = 0;
			while ((line = listReader.read()) != null) {
				if(count++ == 0) {
					continue;
				}
				// Annahme: Die Daten befinden sich in der ersten Spalte
				double value = Double.parseDouble(line.get(0)); // Erste Spalte hat den Index 0
				medianTestwerte.add(value);
			}
		}

		// Konvertieren der Liste in ein Array
		medianTestwerteArray = medianTestwerte.stream().mapToDouble(Double::doubleValue).toArray();

	}

	@Test
	public void testFilteredData() { // Angenommen, DataProcessor.applyMedianFilter() ist die Methode, die die Daten
										// filtert
		double[] actualFilteredData = new DataFilter().filterArrayMedian(medianTestwerteArray);
		// Array der Werte aus Python übertragen.
		double[] expectedMediansArray = { 0.0, 0.0, 1.0, 5.0, 7.0, 13.0, 15.0, 15.0, 23.0, 24.0, 25.0, 25.0, 24.0, 20.0,
				13.0, 7.0, 3.0, 1.0, 0.0 };
		

		assertArrayEquals(expectedMediansArray, actualFilteredData, 0.05);
	}
}
