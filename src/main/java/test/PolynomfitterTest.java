package test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Test;

import backEnd.DataFilter;
import backEnd.Polynomfitter;

public class PolynomfitterTest {

    @Test
    public void testGetError() throws Exception {
        // Erzeuge eine Instanz von Polynomfitter
        Polynomfitter poly = new Polynomfitter();
        DataFilter data = new DataFilter();
        int indexstart = 50;
        int indexend = 900;
        //Polynomgrad(sollte mit dem von Python überein stimmen)-> Polynomfitting01.py
        int grad = 10;


        double actualError =data.getpolynomError(indexstart, indexend, grad);

        // Manueller Fehler des Leastsquaretest (dieser Wert sollte aus Python-Berechnung stammen)
        double expectedError = 45242.215;//<------

        // 5% des erwarteten Fehlers)
        double delta = 0.05 * expectedError; // 5% Abweichung zulassen

        // Vergleiche den tatsächlich berechneten Fehler mit dem erwarteten Fehler
        assertTrue(actualError<=expectedError+delta);
    }
}