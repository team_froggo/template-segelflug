package GUI;

import javax.swing.*;

import backEnd.DataFilter;

import java.awt.Dimension;
import java.awt.Image;

public class GUIapp {

    public static void main(String[] args) {
        try {
            // Set the desired Look and Feel, e.g., Nimbus
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        final View view = new View();
        
        final DataFilter model = new DataFilter();
        final Controller controller = new Controller(view,model);
        //final InfoPanel infopanel = new InfoPanel(model);				//to trash soon

        SwingUtilities.invokeLater(() -> {
            JFrame frame = new JFrame("SkyFrog"); // App Name
            frame.setSize(1400, 1000); // make initial size a little bit larger than min value
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setMinimumSize(new Dimension(1200, 910)); // Set the minimum size to 1200x910 pixels

            // make the icon of the app
            ImageIcon icon = new ImageIcon(GUIapp.class.getResource("/res/frog_logo.png")); // Icon for app
            Image image = icon.getImage();
            frame.setIconImage(image);

            // make panel
            View panel = new View();
            //InfoPanel info = new InfoPanel(model);						//to trash soon
            panel.setDoubleBuffered(true); // reduce flicker
            frame.add(panel);
            frame.setLocationRelativeTo(null); // Center the frame on the screen
            frame.setVisible(true);
            frame.setResizable(true);
            frame.setJMenuBar(panel.getMenuBar());  // Set the menu bar from View to the JFrame
            panel.setController(controller);
           
        });
    }
}
