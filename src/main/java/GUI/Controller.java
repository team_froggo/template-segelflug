package GUI;

import backEnd.DataFilter;

import java.awt.Color;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JPanel;


public class Controller extends JPanel{

	private static final long serialVersionUID = 1L;

	private JFileChooser fcChooser = new JFileChooser();
	
	private final static Color FROG_GREEN = new Color(70,150,70);

	
	private DataFilter model;
	private View view;
	private InfoPanel infopanel;
	

	public Controller(View view, DataFilter model) {
        this.model = model;    
        this.setView(view);
    }
	
	public Controller(InfoPanel infopanel, DataFilter model) {
        this.model = model;    
        this.setInfopanel(infopanel);
    }
	
	public void install(View view) {
		this.setView(view);
	}
	
	public void loadLines() {
		for (int i = 0; i < Data.getInfoElementsAmount(); i++) {
			Data.loadedCheckBox[i] = Data.getSelectedCheckBox()[i];
			Data.getSelectedCheckBox()[i] = 0;
			//System.out.println(Data.getSelectedCheckBox()[i]);
		}
	}
	
	public void setTheme(String theme) {
		System.out.println("setting theme to " + theme);
		
		
		System.out.println("theme string: " + theme);

		
		switch (theme) {
		case "Light":
			Data.setCompassImage("airplane");
			Data.setBackgroundColor(new Color(200, 200, 200));
			Data.setBorderFontColor(new Color(0,0,0));
			Data.setBorderColor(new Color(0,0,0));
			Data.setTargetArms("TargetArms");
			Data.setCGIobj("glider");
			break;
		case "Dark":
			Data.setCompassImage("airplane");
			Data.setBackgroundColor(new Color(50, 50, 50));
			Data.setBorderFontColor(new Color(200, 200, 200));
			Data.setBorderColor(new Color(200, 200, 200));
			Data.setTargetArms("TargetArms");
			Data.setCGIobj("glider");
			break;
		case "Froggo":
			Data.setCompassImage("froggo_noBackground");
			Data.setBackgroundColor(FROG_GREEN);
			Data.setBorderColor(new Color(0,0,0));
			Data.setTargetArms("FrogTargetArms");
			Data.setCGIobj("glider_Frog");
			break;
		case "PikaPika":
			Data.setCompassImage("pika_plane2");
			Data.setBackgroundColor(new Color(250, 214, 30));
			Data.setBorderFontColor(new Color(0,0,0));
			Data.setBorderColor(new Color(0,0,0));
			Data.setTargetArms("pikaTarget");
			Data.setCGIobj("glider_Pika");
			break;
		default:
			Data.setCompassImage("airplane");
			Data.setBackgroundColor(new Color(200, 200, 200));
			Data.setBorderFontColor(new Color(0,0,0));
			Data.setBorderColor(new Color(0,0,0));
			Data.setTargetArms("TargetArms");
			Data.setCGIobj("glider");
			break;
		
		}
		
		
		
		
		System.out.println("compassImage in controller is: "+Data.compassImage);
		System.out.println("BackgroundColor in controller is: "+Data.BackgroundColor);
		
	}

	public void exportPolynom() {
		int ret = fcChooser.showOpenDialog(Controller.this);
		if(ret == JFileChooser.APPROVE_OPTION) {
			File fFile=fcChooser.getSelectedFile();
			System.out.println("File is:"+fFile.getAbsolutePath());
			//String strFilePathString=fFile.getAbsolutePath();
			
			if (Data.viewRange<=Data.getSliderVar()) {
				model.writePolynomtoFile(fFile.toPath() , Data.getSliderVar(),Data.getSliderVar()-Data.viewRange ,Data.getPolynomDegree());
				
			}else {
				model.writePolynomtoFile(fFile.toPath() , Data.getSliderVar(),0 ,Data.getPolynomDegree());
			}
		}
	}
	
	public double getAverageClimbRate() {
		if (Data.viewRange<=Data.getSliderVar()) {
			return model.getaverageClimbrate(Data.getSliderVar()-Data.viewRange, Data.getSliderVar());
			
		}else {
			return model.getaverageClimbrate(0, Data.getSliderVar());
		}
		
	}

	public InfoPanel getInfopanel() {
		return infopanel;
	}

	public void setInfopanel(InfoPanel infopanel) {
		this.infopanel = infopanel;
	}

	public View getView() {
		return view;
	}

	public void setView(View view) {
		this.view = view;
	}
		
		
	
}
