package GUI;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import javax.swing.JPanel;

import backEnd.DataFilter;
import util.Utility;





public class HorizonPanel  extends JPanel {
	DataFilter siFilter;
	private static final long serialVersionUID = 1L;
	
	private double theta = 0;
	private int movementY;
	private int movementX;
	private int size;
	
    public HorizonPanel(DataFilter siFilter) {
		this.siFilter=siFilter;
	}
	
	
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		double[] rollAmount = siFilter.getFilterData(10);		//10 corresponds to roll
        double[] pitchAmount = siFilter.getFilterData(11);		//11 corresponds to pitch
        double rollDeg=rollAmount[Data.getSliderVar()];
        double pitchDeg=pitchAmount[Data.getSliderVar()];
        
        final int ROTATION_DIVISOR = 13;
        final double DEGREE_TO_RADIAN = Math.PI / 180;
        Color GREEN = new Color(30,120,14);
        
        
        //values for little arrow
        int width = getWidth(); // Get the width once to avoid multiple calls
		int[] xPoints = {width / 2 , width / 2 - (20*size)/13, width / 2 + (20*size)/ROTATION_DIVISOR};
		int[] yPoints = {((50+30)*size)/ROTATION_DIVISOR, ((50 + 50)*size)/ROTATION_DIVISOR, ((50 + 50)*size)/ROTATION_DIVISOR};
		int nPoints = 3; // The number of points in the polygon
        
        setMovementY((int)(((-pitchDeg*16)/10)+100));
        theta=DEGREE_TO_RADIAN*rollDeg;
		size= (getWidth()+getHeight())/100;
        

		Graphics2D g2d = (Graphics2D) g;
		

		g2d.setColor(GREEN);					//set color to green
		g2d.fillRect(-0, 0, 3*getWidth(), getHeight());		//draw background
		

		//stuff to rotate around center
		g2d.translate(-getWidth()/2,0);
		g2d.rotate(theta, getWidth(), getHeight()/2);
		
		//draw the sky
		g2d.setColor(Color.BLUE);
		g2d.fillRect(-getWidth()/2, -getHeight() -getMovementY(), 3*getWidth(), 2*getHeight());
		
		//draw horizon line
		g2d.setColor(Color.WHITE);
		g2d.fillRect(-getWidth(), getHeight()-movementY, getWidth()*3, (10*size)/13);
		
		//draw horizontal lines
		g2d.setColor(Color.WHITE);
		g2d.fillRect(getWidth() -(40*size)/13,  getHeight()-((3+50 )*size)/13 -getMovementY(), (80*size)/13, (6*size)/13);
		g2d.fillRect(getWidth() -(40*size)/13,  getHeight()-((3-50 )*size)/13 -getMovementY(), (80*size)/13, (6*size)/13);
		g2d.fillRect(getWidth() -(60*size)/13,  getHeight()-((3+100 )*size)/13 -getMovementY(), (120*size)/13, (6*size)/13);
		g2d.fillRect(getWidth() -(60*size)/13,  getHeight()-((3-100 )*size)/13 -getMovementY(), (120*size)/13, (6*size)/13);
		g2d.fillRect(getWidth() -(40*size)/13,  getHeight()-((3+150 )*size)/13 -getMovementY(), (80*size)/13, (6*size)/13);
		g2d.fillRect(getWidth() -(40*size)/13,  getHeight()-((3-150 )*size)/13 -getMovementY(), (80*size)/13, (6*size)/13);
		g2d.fillRect(getWidth() -(80*size)/13,  getHeight()-((3+200 )*size)/13 -getMovementY(), (160*size)/13, (6*size)/13);
		g2d.fillRect(getWidth() -(80*size)/13,  getHeight()-((3-200 )*size)/13 -getMovementY(), (160*size)/13, (6*size)/13);
		
		g2d.drawString("20", getWidth() -(60*size)/13, getHeight()-((3+100+5 )*size)/13 -getMovementY());
		g2d.drawString("-20", getWidth() -(60*size)/13, getHeight()-((3-100+5 )*size)/13 -getMovementY());
		g2d.drawString("40", getWidth() -(80*size)/13, getHeight()-((3+200+5 )*size)/13 -getMovementY());
		g2d.drawString("-40", getWidth() -(80*size)/13, getHeight()-((3-200+5 )*size)/13 -getMovementY());
		
		
		//stop rotate stuff
		g2d.rotate(-theta, getWidth(), getHeight()/2);
		g2d.translate(+getWidth()/2,0);
		

		//rotating little arrow (has to be separate)
		g2d.rotate(theta, getWidth()/2, getHeight()/2);
		g2d.setColor(Color.WHITE);
		g2d.fillPolygon(xPoints, yPoints, nPoints);
		g2d.rotate(-theta, getWidth()/2, getHeight()/2);
		
		
		//draw the degree marks
		for(double i= -(Math.PI /2-(Math.PI/6));i<=(Math.PI/2)-(Math.PI/6);i+=Math.PI/12){
			g2d.rotate(i, getWidth()/2, getHeight()/2);
			
			g2d.setColor(Color.WHITE);
			//draw degree mark
			g2d.fillRect(getWidth()/2-(2*size)/13, ((50-20)*size)/13, (4*size)/13, (30*size)/13);
			//draw the degree values
			g2d.drawString(String.valueOf(Math.round(Math.toDegrees(i))), getWidth()/2-(2*size)/13-(15*size)/13, ((50-20)*size)/13-(3*size)/13);
			
			g2d.rotate(-i, getWidth()/2, getHeight()/2);
		};
		
		
		
		//draw target
		g2d.setColor(Color.RED);
		g2d.fillOval(getWidth()/2 -(10*size)/13, getHeight()/2 -(10*size)/13, (20*size)/13, (20*size)/13);
		Image targetArmsImage = Utility.loadResourceImage(Data.targetArms);
		g2d.drawImage(targetArmsImage, getWidth()/2-((size*300)/13)/2, getHeight()/2-((size*100)/13)/2, ((size*300)/13), ((size*100)/13), null);
       
		//draw arc thingy
		g2d.setColor(Color.WHITE);
		for(int i=0; i<(size*7)/13; i++) {
			g2d.drawArc(i+(size*50)/13, i+(size*50)/13,              getWidth()-2*(size*50)/13-2*i, getHeight()-2*(size*50)/13-i, 30, 120);
			
		}


	}
	
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(1000, 1000);
	}

	@Override
	public Dimension getMinimumSize() {
		return new Dimension(200, 200);
	}

	public int getMovementX() {
		return movementX;
	}
	public void setMovementX(int movementx) {
		movementX = movementx;
	}
	public void setMovementY(int movementy) {
		movementY = movementy;
	}
	public int getMovementY() {
		return movementY;
	}
}
