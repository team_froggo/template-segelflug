//a pikachu imaga was added to improve the code
package GUI;

import java.awt.Dimension;
import javax.swing.*;

import backEnd.DataFilter;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


public class PolynomPanel extends JPanel {
	
	private static final long serialVersionUID = 1L;
	Data myData = new Data();

    DataFilter siFilter;

    boolean bol = false;

    

	
	private int iPolyDegRange = 60;
	
	private static final Insets INSETS = new Insets(5,5,5,5);

	private JTextField tfTop = new JTextField();
	JTextArea textArea = new JTextArea();
	JScrollPane sclpnTextField = new JScrollPane(textArea);

	
	//txtArea.setLineWrap(true); // enable line wrap
	//txtArea.setWrapStyleWord(true); // enable word wrap
	
    
    private JSlider sldPolyDeg = new JSlider(0, iPolyDegRange);

	String strPoly;
	
	
	public PolynomPanel(DataFilter siFilter) {
		System.out.println("enter polynom panel");
		this.siFilter= siFilter;
		redraw();
	}
	public void redraw() {
		setLayout(new GridBagLayout());
		System.out.println("enter polynom.redraw");
		
	    
	    
	    
	    
	    

		if (Data.viewRange<=Data.getSliderVar()) {
			strPoly=siFilter.getPolynomString(Data.getSliderVar()-Data.viewRange, Data.getSliderVar(), (int)sldPolyDeg.getValue());
		}else {
			strPoly=siFilter.getPolynomString(0, Data.getSliderVar(), (int)sldPolyDeg.getValue());
		}
		Data.setPolynomDegree((int)sldPolyDeg.getValue());
		
        System.out.print("slidervar:"+Data.getSliderVar());

		
		//tfTop.setText(strPoly);		new junk
		textArea.setText(strPoly);
	    textArea.setEditable(false); // set to false if you don't want it to be editable
	    sclpnTextField.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        sclpnTextField.setViewportView(textArea);
        textArea.setEditable(false); // set to false if you don't want it to be editable

		//Polynomfitter siData = new Polynomfitter();
		//strPoly=siData.getPolynom(start, stop, 5);
		//System.out.println("poly:"+strPoly);
        
		sldPolyDeg.setBackground(Data.BackgroundColor);
		
        //tfTop.setFont(Utility.loadFont());
        tfTop.setPreferredSize(new Dimension(100, 20));
        //tfTop.setBorder(MyBorderFactory.createMyBorder("Polynomial"));

        //add(tfTop, new GridBagConstraints(			0, 1, 1, 1, 1, 0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, INSETS, 10, 25));
        add(sclpnTextField, new GridBagConstraints(		0, 1, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, INSETS, 0, 25));
        add(sldPolyDeg, new GridBagConstraints(			0, 2, 1, 1, 1, 0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, INSETS, 10, 25));

        //tfTop.setBorder(MyBorderFactory.createMyBorder("Polygon"));
        
        
        sldPolyDeg.setInverted(true);
        sldPolyDeg.setMinorTickSpacing(1);
        sldPolyDeg.setMajorTickSpacing(10);
        sldPolyDeg.setPaintTicks(true);
        sldPolyDeg.setPaintLabels(true);
        customizeSliderLabels(sldPolyDeg);
        
        textArea.setToolTipText("Displays the polynomial equation based on the slider values.");
        sclpnTextField.setToolTipText("Scrollable area containing the polynomial equation.");
        


        
        sldPolyDeg.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {

				//controller.updatePolySlider();
				
				if (!sldPolyDeg.getValueIsAdjusting()&&!bol) {
					if (Data.viewRange<=Data.getSliderVar()) {
						strPoly=siFilter.getPolynomString(Data.getSliderVar()-Data.viewRange, Data.getSliderVar(), (int)sldPolyDeg.getValue());
					}else {
						strPoly=siFilter.getPolynomString(0, Data.getSliderVar(), (int)sldPolyDeg.getValue());
					}
					Data.setPolynomDegree((int)sldPolyDeg.getValue());
					
			        System.out.print("slidervar:"+Data.getSliderVar());
					
					
			        textArea.setText(strPoly);
				    //textArea.setEditable(false); 																//probably not needed here
				    sclpnTextField.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);	//probably not needed here
				    
			        //sclpnTextField.setViewportView(textArea);
					tfTop.repaint();
					bol = true;
					
	            }else if(!sldPolyDeg.getValueIsAdjusting()&&bol) {
	            	
	            }else if(sldPolyDeg.getValueIsAdjusting()&&bol) {
	            	bol=false;
	            }
				

				

			}
		});

	}
	private static void customizeSliderLabels(JSlider slider) {
	    // Create a map to hold the labels
	    Map<Integer, JComponent> map = new HashMap<>();
	    for (int i = 0; i <= slider.getMaximum(); i += 10) {
	        JLabel label = new JLabel(String.valueOf(i));
	        label.setForeground(Data.BorderFontColor);
	        label.setFont(new Font("Monospaced", Font.BOLD, 12));
	        map.put(i, label);
	    }

	    // Convert the map to a Dictionary to use with the JSlider
	    Dictionary<Integer, JComponent> dictionary = new Hashtable<>();
	    map.forEach(dictionary::put);

	    // Set the label table
	    slider.setLabelTable(dictionary);
	}
	
	public int getSliderDegValue() {
		return (int)sldPolyDeg.getValue();
	}
	public void setTextArea(String str) {
		textArea.setText(str);
	}
	 

}
