package GUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.TitledBorder;
import backEnd.DataFilter;

public class InfoPanel extends JPanel {
	DataFilter siFilter = new DataFilter();
	private static final long serialVersionUID = 1L;

	private static final Insets INSETS = new Insets(5,5,5,5);
	
	final int AVG_CLIMB_RATE_INDX = 16; //pos index 
	
	View view;
	private Controller controller = new Controller(this, siFilter);
	/*
	private Controller controller;
	void setController(Controller controller) {
		this.controller = controller;
	}
	*/

	
	//create multiple JTextFields in a for loop
	private List<JTextField> textFields; // Stores the text fields
	private List<JCheckBox> checkBoxes; // Stores the text fields
	private String[] labels = { "Humidity [%]","Gr. Speed [m/s]", "Air Speed [m/s]", 
								"Temp. [°C]","Pressure [kPa]", "Longitude [°]",
								"Latitude [°]","Altitude [m]","Roll [°]", 
								"Pitch [°]","Yaw [°]", "Course[°]",
								"Energy [m]", "Wind Speed[m/s]", "Wind Direction[°]" ,
								"Climb Speed [m/s]","Avg. Climb Rate [m/s]","time [s]","","","",""};
	
	
	
	public InfoPanel(DataFilter siFilter) {
		this.siFilter=siFilter;
		redraw();	
	}
	
	public static Border createMyBorder(String title, Color color) {
	    Border lineBorder = BorderFactory.createLineBorder(color);  // Change color as necessary
	    Border emptyBorder = BorderFactory.createEmptyBorder(0, 0, 0, 0); // Adjust padding inside the border
	    Border titledBorder = BorderFactory.createTitledBorder(lineBorder, title);
	    //set titleBorder color
	    
	    return new CompoundBorder(titledBorder, emptyBorder);
	}
	
	private Border createMyBorder(String title, Color titleColor, Color borderColor) {
	    Border lineBorder = BorderFactory.createLineBorder(borderColor);
	    TitledBorder titledBorder = new TitledBorder(lineBorder, title);
	    
	    // Retrieve the current font from the border
	    Font currentFont = titledBorder.getTitleFont();
	    
	    // If a font exists, modify it to set the size to 10
	    if (currentFont != null) {
	        titledBorder.setTitleFont(currentFont.deriveFont(currentFont.getStyle(), 10f));  // Set font size to 10
	    } else {
	        // If no font has been set, create a new one with size 10
	        titledBorder.setTitleFont(new Font("Serif", Font.PLAIN, 10));
	    }
	    
	    titledBorder.setTitleColor(titleColor);
	    return titledBorder;
	}
	private Border createMyBorder(String title, Color titleColor, Color borderColor, boolean isBold) {
        Border lineBorder = BorderFactory.createLineBorder(borderColor);

        TitledBorder titledBorder = new TitledBorder(lineBorder, title);
        Font currentFont = titledBorder.getTitleFont();
        if (currentFont == null) {
            currentFont = new Font("Dialog", Font.PLAIN, 13);  // Default font if no font is set
        }
        titledBorder.setTitleFont(currentFont.deriveFont(isBold ? Font.BOLD : Font.PLAIN));  // Bold or Plain based on isBold
        titledBorder.setTitleColor(titleColor);

        return titledBorder;
    }
	
	
	public List<JTextField> getTextFields() {
        return textFields; // Accessor to retrieve the list of text fields
    }


    
    public void redraw() {
    	Data data = new Data();
    	//TimeGraph graph = new TimeGraph();
    	
        
    	removeAll();
    
    	textFields = new ArrayList<>();
		checkBoxes = new ArrayList<>();
        //setLayout(new GridLayout(numberOfFields, 1)); // Set layout
		setLayout(new GridBagLayout());
		
		setBackground(Data.BackgroundColor);
		
		int rows = 3; // Now 5 rows
	    int cols = 6; // and 3 columns
	    

		for (int j = 0; j < cols; j++) {
		    for (int i = 0; i < rows; i++) {
		        final int finalJ = j; // Final copy of j
		        final int finalI = i; // Final copy of i
		        final int finalIndex = (rows * finalJ + finalI);
		        
		        double[] dataSet = null;
	        	
		        
	        	dataSet = siFilter.getFilterData(finalIndex+2);	//+2 because the first 2 elements of the data set are the time and we don't want that
				
	        	JCheckBox checkBox = new JCheckBox();
						
	        	String textFieldVar;
		        if(finalIndex==AVG_CLIMB_RATE_INDX){
		        	textFieldVar=String.format("%.6f",controller.getAverageClimbRate());
		      
		        	
		        }else if(finalIndex>AVG_CLIMB_RATE_INDX) {
		        	dataSet = siFilter.getFilterData(-1);
		        	textFieldVar = String.format("%.6f", dataSet[Data.getSliderVar()]);	
		        }else {
		        	textFieldVar = String.format("%.6f", dataSet[Data.getSliderVar()]);	
			        add(checkBox, new GridBagConstraints(2 * finalJ, finalI, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, INSETS, 0, 0));

		        }
		        
		        
		        JTextField textField = new JTextField(textFieldVar,10);
		        

		        
		        checkBoxes.add(checkBox);
		        checkBox.setBackground(Data.BackgroundColor);
		        textField.setBackground(Data.BackgroundColor);
		        textField.setForeground(Data.BorderFontColor); 
		        textField.setEditable(false);
		        System.out.println("finalIndex:"+finalIndex+""+labels[finalIndex]);
		        
		        textField.setBorder(createMyBorder(labels[finalIndex],Data.BorderFontColor ,Data.BorderColor));	
		        textFields.add(textField);
		        
		        add(textField, new GridBagConstraints(2 * finalJ + 1, finalI, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, INSETS, 0, 0));
		        
		        checkBox.addActionListener(this::checkBoxAction);
		        
		        checkBox.addActionListener(new ActionListener() {
		            @Override
		            public void actionPerformed(ActionEvent e) {
		                boolean isSelected = checkBox.isSelected();
		                Color borderColor = isSelected ? Color.RED : Data.BorderColor;
		                Data.getSelectedCheckBox()[finalIndex]=isSelected ? 1:0;	
		                
		                System.out.println("\n\\n");
		                for(int k=0;k<Data.getSelectedCheckBox().length;k++) {
		                	System.out.println("checkboxes:"+Data.getSelectedCheckBox()[k]);
		                }
		                
		                textField.setForeground(borderColor);
		                textField.setBorder(createMyBorder(labels[finalIndex], borderColor,borderColor, true));
		                Data.setyData(data.allData[finalIndex]);		
		                Data.setDatapos(finalIndex);
		                
		            }
		        });
		    }
		
			
			
			
		}
		revalidate();  // Recalculate the layout
	    repaint();  // Repaint the panel;
    }
    
    private void checkBoxAction(ActionEvent e) {
    }
    
    @Override
	public Dimension getPreferredSize() {
		return new Dimension(800, 400);
	}

	@Override
	public Dimension getMinimumSize() {
		return new Dimension(800, 250);
	}
}
