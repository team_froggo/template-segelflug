package GUI;

import java.awt.Color;
import java.nio.file.Path;

//import java.util.Arrays;

public class Data {
	
	//data that gets loaded in case no file is selected (used only in info panel)
	public double[][] allData = new double[][]{
	    {23229387.0, 23429314.0, 23629397.0, 23829100.0, 24029192.0, 24229541.0, 24429303.0, 24629747.0, 24829417.0, 25029523.0, 25229078.0, 25429173.0, 25629509.0, 25829383.0, 26029672.0},
	    {253978200.0, 253978400.0, 253978600.0, 253978800.0, 253979000.0, 253979200.0, 253979400.0, 253979600.0, 253979800.0, 253980000.0, 253980200.0, 253980400.0, 253980600.0, 253980800.0, 253981000.0},
	    {20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0},
	    {0.0, 0.001, 0.0, 0.001, 0.001, 0.0, 0.0, 0.001, 0.0, 0.001, 0.071, 0.445, 1.16, 1.764, 2.168},
	    {0.0220617, 0.0220617, 0.0220617, 0.0220617, 0.0220617, 0.0220617, 0.0220617, 0.0220617, 0.0220617, 0.0220617, 0.0220617, 0.0220617, 0.0220617, 0.0220617, 0.0220617},
	    {28.83, 28.85, 28.87, 28.89, 28.91, 28.94, 28.96, 28.98, 29.0, 29.02, 29.04, 29.06, 29.08, 29.1, 29.12},
	    {96097.43, 96094.63, 96095.97, 96096.37, 96093.23, 96093.6, 96097.28, 96096.64, 96095.97, 96096.43, 96096.73, 96096.67, 96095.48, 96095.18, 96094.3},
	    {8.257222, 8.257222, 8.257222, 8.257222, 8.257222, 8.257222, 8.257222, 8.257222, 8.257222, 8.257222, 8.257222, 8.2572226, 8.2572247, 8.2572288, 8.2572342},
	    {47.3425159, 47.3425159, 47.3425159, 47.3425159, 47.3425159, 47.3425159, 47.3425159, 47.3425159, 47.3425159, 47.3425159, 47.3425159, 47.3425159, 47.3425159, 47.3425159, 47.3425158},
	    {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.0299999999999727},
	    {-8.49, -8.49, -8.49, -8.49, -8.49, -8.49, -8.49, -8.49, -8.49, -8.49, -8.52, -8.74, -9.17, -10.05, -9.28},
	    {3.89, 3.89, 3.89, 3.89, 3.89, 3.89, 3.89, 3.89, 3.89, 3.89, 3.24, -1.58, -11.57, -20.06, -15.03},
	    {91.39, 91.39, 91.39, 91.39, 91.39, 91.39, 91.39, 91.39, 91.4, 91.4, 91.51, 91.88, 92.39, 92.6, 91.32},
	    {87.39772, 282.6938, 83.25962, 90.36563, 100.3574, 160.3473, 11.20741, 89.68102, 309.5583, 297.5623, 89.07954, 90.40578, 90.93217, 91.28733, 91.0817},
	    {87.39772, 282.6938, 83.25962, 90.36563, 100.3574, 160.3473, 11.20741, 89.68102, 309.5583, 297.5623, 89.07954, 90.40578, 90.93217, 91.28733, 91.0817},
	    {87.39772, 282.6938, 83.25962, 90.36563, 100.3574, 160.3473, 11.20741, 89.68102, 309.5583, 297.5623, 89.07954, 90.40578, 90.93217, 91.28733, 91.0817}
	};
	
	//gets used in info panel but not quite sure why
	private static double[] yData = new double[] {};
	private static int Datapos=2;
	
	private static int sliderVar=100;
	public static int viewRange=100;
	
	public static int infoElementsAmount=18;
	public static int polynomDegree;
	public static Path filePath;
	
	public static String compassImage = "airplane";
	public static Color BackgroundColor = new Color(200, 200, 200);
	public static Color BorderFontColor = new Color(0, 0, 0);
	public static Color BorderColor = new Color(0, 0, 0);
	public static String targetArms = "TargetArms";
	public static String CGIobj = "glider";
	
	
	
	public static Path getFilePath() {
		return filePath;
	}
	public static void setFilePath(Path filePath) {
		Data.filePath = filePath;
	}
	public static int getPolynomDegree() {
		return polynomDegree;
	}
	public static void setPolynomDegree(int polynomDegree) {
		Data.polynomDegree = polynomDegree;
	}
	public static String getCompassImage() {
		return compassImage;
	}
	public static void setCompassImage(String compassImage) {
		Data.compassImage = compassImage;
	}
	public static Color getBackgroundColor() {
		return BackgroundColor;
	}
	public static void setBackgroundColor(Color backgroundColor) {
		BackgroundColor = backgroundColor;
	}
	public static Color getBorderFontColor() {
		return BorderFontColor;
	}
	public static void setBorderFontColor(Color borderFontColor) {
		BorderFontColor = borderFontColor;
	}
	public static Color getBorderColor() {
		return BorderColor;
	}
	public static void setBorderColor(Color borderColor) {
		BorderColor = borderColor;
	}
	public static String getTargetArms() {
		return targetArms;
	}
	public static void setTargetArms(String targetArms) {
		Data.targetArms = targetArms;
	}
	public static String getCGIobj() {
		return CGIobj;
	}
	public static void setCGIobj(String cGIobj) {
		CGIobj = cGIobj;
	}

	//private static int[] selectedCheckBox= 	{0,0,0,0,0	,0,0,0,0,0	,0,0,0,0,0	,0,0,0};//18 spaces //the two time datasets have phantom checkboxes
	public static int[] loadedCheckBox= 	{0,0,0,0,0	,0,0,0,0,0	,0,0,0,0,0	,0,0,0};//18 spaces //the two time datasets have phantom checkboxes
	
	private static int[] selectedCheckBox = new int[infoElementsAmount];

	
	public static int[] getSelectedCheckBox() {
		return selectedCheckBox;
	}
	public static void setSelectedCheckBox(int[] selectedCheckBox) {
		Data.selectedCheckBox = selectedCheckBox;
	}
	
	public static int iSpeedVal=1;
	
	
	public static int getInfoElementsAmount() {
		return infoElementsAmount;
	}
	
	public static int getSliderVar() {
		return sliderVar;
	}
	public static void setSliderVar(int sliderVar) {
		Data.sliderVar = sliderVar;
	}
	
	public static double[] getyData() {
		return yData;
	}
	public static void setyData(double[] yData) {
		Data.yData = yData;
	}
	public static int getDatapos() {
		return Datapos;
	}
	public static void setDatapos(int datapos) {
		Datapos = datapos;
	}
	
	
}
