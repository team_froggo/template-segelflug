package GUI;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import javax.swing.JPanel;
import util.Utility;
//import backEnd.SuperCSVLoader;
import backEnd.DataFilter;



public class CompassPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	DataFilter siFilter;
	
	private int size = 8;
	private int skyWidth;
	private int skyHeight;
	private int movementY;
	private int movementX;
	private int shiftAmount;
	private double theta = 0;

	

	public CompassPanel(DataFilter siFilter) {
		this.siFilter=siFilter;
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
        double[] siRotDataset = siFilter.getFilterData(12);	//12 equals to the rotation degree
        double rotationDeg=siRotDataset[Data.getSliderVar()];
        
        theta=(Math.PI*rotationDeg)/180;
       
		setSkyWidth(2*getWidth());
		setSkyHeight(1*getHeight());
		
		size= (getWidth()+getHeight())/100;
        
		//values for little arrow
		int width = getWidth(); // Get the width once to avoid multiple calls
		int[] xPoints = {width / 2 , width / 2 - (20*size)/13, width / 2 + (20*size)/13};
		int[] yPoints = {((50+30)*size)/13, ((50 + 50)*size)/13, ((50 + 50)*size)/13};
		int nPoints = 3; // The number of points in the polygon (polygon being the little arrow)

		Graphics2D g2d = (Graphics2D) g;
		
		//create the background
		g2d.setColor(Color.BLACK);
		g2d.fillRect(-0, 0, getWidth(), getHeight());
		
		//rotate
		g2d.rotate(-theta, getWidth()/2, getHeight()/2);
		
		//draw the degree marks
		for(double i= 0;i<=2*Math.PI;i+=Math.PI/12){
			//set color to red for every 90 degrees
			if(Math.round(Math.toDegrees(i))==90 || Math.round(Math.toDegrees(i))==180 || Math.round(Math.toDegrees(i))==270 || Math.round(Math.toDegrees(i))==360){
				g2d.setColor(Color.RED);
			}else {
				g2d.setColor(Color.WHITE);
			}
			
			g2d.rotate(i, getWidth()/2, getHeight()/2);		//rotate amount
			
			//degree marks
			g2d.fillRect(getWidth()/2-(2*size)/13, ((70-30)*size)/13, (4*size)/13, (30*size)/13);
			//degree values
			g2d.drawString(String.valueOf(Math.round(Math.toDegrees(i))), getWidth()/2-(0*size)/13-(25*size)/13, ((70-30)*size)/13-(3*size)/13);
			
			g2d.rotate(-i, getWidth()/2, getHeight()/2);	//rotate back same amount
		};
		
		
		//little white arrow
		g2d.setColor(Color.WHITE);
		g2d.fillPolygon(xPoints, yPoints, nPoints);
		
		//rotate back to original position
		g2d.rotate(theta, getWidth()/2, getHeight()/2);
		
		//image or aircraft
		Image airPlaneImage = Utility.loadResourceImage(Data.compassImage);
		g2d.drawImage(airPlaneImage, getWidth()/2-((size*250)/13)/2, getHeight()/2-((size*250)/13)/2, ((size*250)/13), ((size*250)/13), null);
	}
	

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(400, 400);
	}

	@Override
	public Dimension getMinimumSize() {
		return new Dimension(200, 200);
	}

	public int getSkyWidth() {
		return skyWidth;
	}

	public void setSkyWidth(int skywidth) {
		this.skyWidth = skywidth;
	}

	public int getSkyHeight() {
		return skyHeight;
	}

	public void setSkyHeight(int skyheight) {
		this.skyHeight = skyheight;
	}

	public int getMovementY() {
		return movementY;
	}

	public void setMovementY(int movementy) {
		movementY = movementy;
	}

	public int getMovementX() {
		return movementX;
	}

	public void setMovementX(int movementx) {
		movementX = movementx;
	}

	public int getShift() {
		return shiftAmount;
	}

	public void setShift(int shiftamount) {
		this.shiftAmount = shiftamount;
	}

}