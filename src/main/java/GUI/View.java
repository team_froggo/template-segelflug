package GUI;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSlider;
import javax.swing.Timer;
import javax.swing.border.Border;
import javax.swing.border.MatteBorder;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import backEnd.DataFilter;
import flightpanel.map.Coordinate;

public class View extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	DataFilter siFilter = new DataFilter();
	public final static int iSliderRange = 1000;
	

	public static String selectedFileString;

	public static int sliderValue = 10;
	private JSlider sldTime = new JSlider(4, iSliderRange);			//why the heck is there a 4??? have to set relative start value
	//private JSlider sldViewRange = new JSlider(0, iSliderRange);
	private JSlider sldViewRange = new JSlider(JSlider.VERTICAL, 0, iSliderRange, 10);
	
	
	
	private static final Insets INSETS = new Insets(5, 5, 5, 5);
	private Timer timer;
	public static float speed = 100;

	private HorizonPanel Horizont = new HorizonPanel(siFilter);
	private CompassPanel Compass = new CompassPanel(siFilter);
	private InfoPanel Info = new InfoPanel(siFilter);

	// stolen code from teacher
	private StolenFligtPanel flightPanel;
	private JProgressBar jpLoading = new JProgressBar();
	//private JSlider sliderRoll = new JSlider(0, 360);
	//private JSlider sliderPitch = new JSlider(0, 360);
	//private JSlider sliderYaw = new JSlider(0, 360);

	private JSlider sliderProgress = new JSlider(0, 1);

	private JComboBox<String> cbPath = new JComboBox<>();
	private JComboBox<String> cbSpeed = new JComboBox<>();

	private PolynomPanel pnlPolynom = new PolynomPanel(siFilter);
	// private ThemePanel Theme = new ThemePanel();
	private GraphPanel Graph = new GraphPanel(siFilter);
	// private Slider Slider = new Slider();
	private JPanel emptyPanel = new JPanel();
	private JPanel pnlEmpty2 = new JPanel();
	private JButton btnLoadLine = new JButton("load lines");
	private JButton btnPlay = new JButton("Play");

	//private JComboBox<String> cbTheme = new JComboBox<>();
	
	private JFileChooser fcChooser = new JFileChooser();

	private final Insets insets = new Insets(2, 2, 2, 2);

	// preset theme values
	public static Color BackgroundColor = new Color(200, 200, 200);
	public static Color BorderFontColor = new Color(0, 0, 0);
	public static Color BorderColor = new Color(0, 0, 0);
	public static String targetArms = "TargetArms";
	public static String compassImage = "airplane";
	public static String CGIobj = "glider";
	
	// make string list for drop down menu

	Path dirPath = Paths.get("template-segelflug", "src", "main", "resources");

	
	Border border = BorderFactory.createLineBorder(Color.RED, 1);	//Junk code "please delete me"
	Border gridLineBorder = new MatteBorder(1, 1, 1, 1, Color.GRAY);  // Color and thickness of the border
	
	private JMenuBar menuBar;
	
	//MVC stuff
	private Controller controller;
	void setController(Controller controller) {
		this.controller = controller;
	}
	//private DataFilter siFilte;
	//////////////////////////
	//private JFrame frame;
    //private JMenuBar menuBar;
	private JMenu speedMenu2;
	private List<JMenuItem> speedItems2;
	
	public View() {
		init();
		setupbtnPlay();
		initializeTimer();
		
		//Setup menu bar
		this.menuBar = new JMenuBar();  // Initialize the class member, not a local variable
	    JMenu fileMenu = new JMenu("File");
	    JMenu exportMenu = new JMenu("Export");
	    JMenuItem exportAsPdf = new JMenuItem("Export as PDF");
	    JMenuItem exportAsTxt = new JMenuItem("Export as TXT");
	    JMenuItem exportPolynom = new JMenuItem("Export Polynom");
	    
	    exportPolynom.addActionListener(new ActionListener() {
	    		@Override
            public void actionPerformed(ActionEvent e) {
                controller.exportPolynom();
            }
	    });
	    
	    
	    
	    JMenu importMenu = new JMenu("Import Files");
	    
	    JMenu settingsMenu = new JMenu("Settings");
	    
	   
        
        
        JMenu themesMenu = new JMenu("Themes");
        JMenuItem themeLight = new JMenuItem("Light");
        JMenuItem themeDark = new JMenuItem("Dark");
        JMenuItem themeFroggo = new JMenuItem("Froggo");
        JMenuItem themePikaPika = new JMenuItem("PikaPika");
        
	    JMenu infoMenu = new JMenu("Info");
	    
	    //checking for themes
	    themeLight.addActionListener(new ActionListener() {
	    		@Override
            public void actionPerformed(ActionEvent e) {
                controller.setTheme("Light");  // Trigger the theme change in the controller
                updateTheme();      
            }
	    });
	    themeDark.addActionListener(new ActionListener() {
	        @Override
	        public void actionPerformed(ActionEvent e) {
	            controller.setTheme("Dark");  // Trigger the theme change in the controller
	            updateTheme();                
	        }
	    });
	    themeFroggo.addActionListener(new ActionListener() {
	        @Override
	        public void actionPerformed(ActionEvent e) {
	            controller.setTheme("Froggo");  // Trigger the theme change in the controller
	            updateTheme();               
	        }
	    });
	    themePikaPika.addActionListener(new ActionListener() {
	        @Override
	        public void actionPerformed(ActionEvent e) {
	            controller.setTheme("PikaPika");  // Trigger the theme change in the controller
	            updateTheme();              
	        }
	    });
	    
     
        
	    

	    exportAsPdf.addActionListener(e -> System.out.println("Export as PDF clicked"));
	    exportAsTxt.addActionListener(e -> System.out.println("Export as TXT clicked"));
	 
        
        exportMenu.add(exportAsPdf);
	    exportMenu.add(exportAsTxt);
	    exportMenu.add(exportPolynom);
	    
	    /*
	    
	    //settingsMenu.add(speedMenu2);
	    settingsMenu.add(themesMenu);
	    
	   
         */
	    
        themesMenu.add(themeLight);
        themesMenu.add(themeDark);
        themesMenu.add(themeFroggo);
        themesMenu.add(themePikaPika);
       
        
	    fileMenu.add(exportMenu);
	    fileMenu.add(importMenu);
	    //infoMenu.add(exportMenu);
	    this.menuBar.add(fileMenu);
	    this.menuBar.add(settingsMenu);
	    this.menuBar.add(infoMenu);
	    
	   
        

	    
        speedMenu2 = new JMenu("Speed");
        speedItems2 = new ArrayList<>();
        setupSpeedItems2();  // Method to populate speedMenu2

        // Adding submenus to settingsMenu
        //settingsMenu.add(speedMenu);
        settingsMenu.add(speedMenu2);
        settingsMenu.add(themesMenu);
	    
        System.out.println("View");
        init();
        drawView();
        
        
        Path dirPath = Paths.get(System.getProperty("user.dir"), "src", "main", "resources");
		//System.out.println("Absolute Path: " + dirPath.toAbsolutePath().toString());

		if (Files.exists(dirPath) && Files.isDirectory(dirPath)) {
			List<String> csvFiles = new ArrayList<>();
			try (DirectoryStream<Path> stream = Files.newDirectoryStream(dirPath, "*.csv")) {
				for (Path filePath : stream) {
					csvFiles.add(filePath.getFileName().toString());
				}
			} catch (IOException | DirectoryIteratorException e) {
				System.err.println("Error reading the directory: " + e.getMessage());
				e.printStackTrace();
			}

			//System.out.println("Files found: " + csvFiles.size());
			csvFiles.forEach(System.out::println);
		} else {
			System.err.println("Directory does not exist or is not a directory: " + dirPath.toAbsolutePath());
		}

		// This list will hold the names of all CSV files
		List<String> csvFiles = new ArrayList<>();

		try (DirectoryStream<Path> stream = Files.newDirectoryStream(dirPath, "*.csv")) {
			for (Path filePath : stream) {
				// Add the file name to the list
				csvFiles.add(filePath.getFileName().toString());
			}
		} catch (IOException | DirectoryIteratorException e) {
			System.err.println("Error reading the directory: " + e);
		}

		//System.out.println("filename: start");
		// Converting List to Array
		String[] csvFileArray = csvFiles.toArray(new String[0]);
		for (int i = 0; i < csvFileArray.length; i++) {
			//System.out.println("filename: " + csvFileArray[i]);
		}
		cbPath.addItem("load csv file");
		// Theme Section
		for (int i = 0; i < csvFileArray.length; i++) {
			cbPath.addItem(csvFileArray[i]);
		}
		cbPath.addItem("other path/ file");

		// dropdown box action listener
		cbPath.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				@SuppressWarnings("unchecked")
				JComboBox<String> comboBox = (JComboBox<String>) e.getSource();
				
				
				
				if((String) comboBox.getSelectedItem()=="other path/ file"){
					//FileChooserDemo.ChooseFile();
					int ret = fcChooser.showOpenDialog(View.this);
					if(ret == JFileChooser.APPROVE_OPTION) {
						File fFile=fcChooser.getSelectedFile();
						System.out.println("File is:"+fFile.getAbsolutePath());
						siFilter.changepath(fFile.toPath());
					}
				}else {
					String selectedFileString = (String) comboBox.getSelectedItem();
					//System.out.println("Selected CSV File: " + selectedFileString);
					//System.out.println("selected path: " + selectedFileString);
					siFilter.changepath(selectedFileString);
					// siFilter.changepath("src\\main\\resources\\data.csv");
					//System.out.println("dataset length: " + siFilter.getDatafromCSV(0).length);
				}
				
				

				updateView();
				update3DView();
				repaint();

			}
		});
        
		flightPanel.init();
		
        btnLoadLine.addActionListener(new ActionListener() {
        	@Override
			public void actionPerformed(ActionEvent e) {
				// if source is load button is pressed
				if (e.getSource() == btnLoadLine) {
					controller.loadLines();	
					updateInfo();
					updateGraph();
				}

			}
		});
        
        //big horizontal slider
        sldTime.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				sldTime.setMaximum(iSliderRange/Data.iSpeedVal); 
				flightPanel.setAircraftPosition(sldTime.getValue()*Data.iSpeedVal);
				JSlider source = (JSlider) e.getSource();
				
				
				//int value = source.getValue();
				//Data data = new Data();
				Data.setSliderVar(source.getValue());
				// sliderValue = value;
				//System.out.println("Slider value: " + data.sliderVar);

				
				updateView();
				
				if (sldTime.getValue() >= sldTime.getMaximum()){
					timer.stop(); // Stop the timer
					btnPlay.setText("Play"); // Update button text to "Play"
				}
			}
		});
        
        sldViewRange.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				
				Data.viewRange = (int) (((sldViewRange.getValue() / (float) iSliderRange))* siFilter.getFilterData(0).length); // ((val)/(tot val))*aviable to view val
				updateView();
			}
		});
        
    }
	
	public void init() {
		System.out.println("init");
		Data.setSliderVar(iSliderRange/2);
		Data.viewRange = (int) (((sldViewRange.getValue() / (float) iSliderRange))* siFilter.getFilterData(0).length);
		updateGraph();
		updateView();
	}
	public void drawView() {
		
		setBackground(Data.BackgroundColor);
		Info.setBackground(Data.BackgroundColor);
		pnlPolynom.setBackground(Data.BackgroundColor);
		emptyPanel.setBackground(Data.BackgroundColor);
		pnlEmpty2.setBackground(Data.BackgroundColor);

		sldTime.setBackground(Data.BackgroundColor);
		sldViewRange.setBackground(Data.BackgroundColor);
		
	
		setLayout(new GridBagLayout());
		
		jpLoading.setValue(0);
		flightPanel = new StolenFligtPanel(pct -> {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					//System.out.println("setting value: " + pct);
					jpLoading.setValue((int) pct + 1);
				}
			});

		});
		
		repaint();
		
		// add white empty panel
		add(flightPanel, new GridBagConstraints(	0, 0, 4, 3, 100, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH,INSETS, 0, 0));
		add(Horizont, new GridBagConstraints(		4, 0, 3, 1, 0, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE,insets, 0, 0));
		add(Compass, new GridBagConstraints(		4, 1, 3, 1, 0, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE,new Insets(0, 0, 0, 0), 0, 0));
		//add(emptyPanel, new GridBagConstraints(3, 2, 1, 1, 0, 0, GridBagConstraints.CENTER,GridBagConstraints.HORIZONTAL, INSETS, 0, 0));
		add(cbPath, new GridBagConstraints(			4, 2, 3, 1, 1, 1, GridBagConstraints.EAST, GridBagConstraints.NONE,insets, 0, 0));
		add(pnlEmpty2, new GridBagConstraints(		5, 2, 1, 1, 1, 0, GridBagConstraints.CENTER,GridBagConstraints.HORIZONTAL, INSETS, 0, 0));

		
		
		add(sldTime, new GridBagConstraints(			0, 3, 6, 1, 0, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,INSETS, 0, 0));
		add(Graph, new GridBagConstraints(			0, 4, 6, 3, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, INSETS,0, 0));
		add(sldViewRange, new GridBagConstraints(	6, 5, 1, 1, 0, 7, GridBagConstraints.WEST,GridBagConstraints.VERTICAL, INSETS, 0, 0));
		add(emptyPanel, new GridBagConstraints(		5, 6, 1, 1, 0, 1, GridBagConstraints.EAST,GridBagConstraints.VERTICAL, INSETS, 0, 0));

		add(pnlPolynom, new GridBagConstraints(		3, 7, 4, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.BOTH,INSETS, 0, 0));
		add(Info, new GridBagConstraints(			0, 7, 3, 1, 0, 1, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, INSETS,0, 0));
		
		add(btnPlay, new GridBagConstraints(		0, 8, 1, 1, 0, 0, GridBagConstraints.WEST,GridBagConstraints.NONE, INSETS, 0, 0));
		//add(cbSpeed, new GridBagConstraints(		1, 8, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE,INSETS, 0, 0));
		add(btnLoadLine, new GridBagConstraints(	2, 8, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE,INSETS, 0, 0));
		//add(cbTheme, new GridBagConstraints(		6, 8, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE,INSETS, 0, 0));
		
		//Tooltip information
		//flightPanel.setToolTipText("Shows the current position of the aircraft in 3D space");
		btnPlay.setToolTipText("Button to start/stop the animation the animation");
		btnLoadLine.setToolTipText("Button to load the selected graph lines");
		cbPath.setToolTipText("Button to load the selected CSV file");
		//cbTheme.setToolTipText("Button to change the theme");
		sldTime.setToolTipText("Slider to select the point in time");
		cbSpeed.setToolTipText("Selects the speed of the animation");
		sldViewRange.setToolTipText("Slider to select the time frame to view");
		
		Info.setBorder(createMyBorder("Information Panel", BorderFontColor, BorderColor));
		pnlPolynom.setBorder(createMyBorder("Polynom", BorderFontColor, BorderColor));
		
		sldViewRange.setInverted(true);
		sldViewRange.setMinorTickSpacing(50);
		sldViewRange.setMajorTickSpacing(200);
		sldViewRange.setPaintTicks(true);
		sldViewRange.setPaintLabels(true);
		customizeSliderLabels(sldViewRange);
	}
	
	public JMenuBar getMenuBar() {
	    return this.menuBar; // Ensure this.menuBar is accessible
	}
	
	private Border createMyBorder(String title, Color titleColor, Color borderColor) {
		Font borderFont = new Font("Serif", Font.BOLD, 24); // Standard font for all borders
		Border lineBorder = BorderFactory.createLineBorder(borderColor);
		TitledBorder titledBorder = new TitledBorder(lineBorder, title);
		titledBorder.setTitleFont(borderFont);
		titledBorder.setTitleColor(titleColor);
		return titledBorder;
	}
	private static void customizeSliderLabels(JSlider slider) {
        slider.setLabelTable(slider.createStandardLabels(200));
        @SuppressWarnings("unchecked")
		java.util.Dictionary<Integer, JComponent> labelTable = slider.getLabelTable();
        labelTable.elements().asIterator().forEachRemaining(label -> {
            if (label instanceof JLabel) {
                ((JLabel) label).setForeground(Data.BorderFontColor);
                ((JLabel) label).setFont(new Font("Monospaced", Font.BOLD, 12));
            }
        });
    }
	public void updateGraph() {
		System.out.println("updateGraph");
		Graph.redraw();
	}
	public void updatePolynom() {
		System.out.println("updatePolynom");
        pnlPolynom.redraw();
        pnlPolynom.repaint();
    }
	public void updateInfo() {
		System.out.println("updateInfo");
		Info.repaint();
        Info.redraw();
    }
	
	public void updateTheme() {
		setBackground(Data.BackgroundColor);
		Info.setBackground(Data.BackgroundColor);
		Graph.setBackground(Data.BackgroundColor);
		pnlPolynom.setBackground(Data.BackgroundColor);
		emptyPanel.setBackground(Data.BackgroundColor);
		sldTime.setBackground(Data.BackgroundColor);
		sldViewRange.setBackground(Data.BackgroundColor);
		pnlEmpty2.setBackground(Data.BackgroundColor);
		
		Info.setBorder(createMyBorder("Information Panel", Data.BorderFontColor, Data.BorderColor));
		pnlPolynom.setBorder(createMyBorder("Polynom", Data.BorderFontColor, Data.BorderColor));
		
		Info.repaint();
		Info.redraw();
		pnlPolynom.redraw();
		// Graph.repaint();
		Graph.redraw();
		// flightPanel.init();
		flightPanel.repaint();
		flightPanel.revalidate();
		flightPanel.updateAircraftModel();
		
		Horizont.repaint();
		Compass.repaint();
		
		customizeSliderLabels(sldViewRange);
		repaint();
	}
	
	public void updateView() {
		Horizont.repaint();
		Compass.repaint();
		Graph.redraw();
		Info.repaint();
		Info.redraw();
		if(!sldTime.getValueIsAdjusting()) {
			pnlPolynom.redraw();
			pnlPolynom.repaint();
		}
		
		//System.out.println("\nrepeint pnlPolynom");
	}
	
	public void setupbtnPlay() {
		btnPlay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Check if the timer is currently running
				if (timer.isRunning()) {
					timer.stop(); // Stop the timer
					btnPlay.setText("Play"); // Update button text to "Play"
				} else {
					try {
						// Read and parse the speed value only when starting the timer
						// String speedText = (String) cbSpeed.getSelectedItem(); //unnecessary code
						// relics
						float newSpeed = speed;
						if ((newSpeed > 0) ){
							speed = newSpeed;
							int newDelay = (int) (100 / speed);
							//System.out.println("speed: " + speed);
							//System.out.println("New delay: " + newDelay);
							timer.setDelay(newDelay); // Apply the new delay
							timer.start(); // Start the timer
							btnPlay.setText("Pause"); // Update button text to "Pause"
						} else {
							JOptionPane.showMessageDialog(View.this, "Speed must be greater than zero.",
									"Invalid Input", JOptionPane.ERROR_MESSAGE);
						}
					} catch (NumberFormatException ex) {
						JOptionPane.showMessageDialog(View.this, "Please enter a valid number for speed.",
								"Input Error", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
	}
	private void initializeTimer() {

		// 100 ms interval and the action to perform
		timer = new Timer(100, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (sldTime.getValue() < sldTime.getMaximum()) {
					sldTime.setValue(sldTime.getValue() + 1);
				} else {
					timer.stop(); // Optionally stop the timer if max is reached
				}
			}
		});
	}
	
	 private void setupSpeedItems2() {
        int[] speeds = {1, 2, 3, 4, 10};
        for (int speed : speeds) {
            JMenuItem item = new JMenuItem(speed + "x Speed");
            item.addActionListener(this::handleSpeedChange);
            speedMenu2.add(item);
            speedItems2.add(item);
        }
        updateSpeedMenu();
    }

    private void handleSpeedChange(ActionEvent e) {
        JMenuItem source = (JMenuItem) e.getSource();
        Data.iSpeedVal = (int)(Integer.parseInt(source.getText().split("x")[0].trim()));
        System.out.println("speed: " + Data.iSpeedVal);
        updateSpeedMenu();
    }

    private void updateSpeedMenu() {
        for (JMenuItem item : speedItems2) {
            int itemSpeed = Integer.parseInt(item.getText().split("x")[0].trim());
            if (itemSpeed == Data.iSpeedVal) {
                item.setBackground(Color.RED);
                item.setForeground(Color.RED);
            } else {
                item.setBackground(null);
                item.setForeground(null);
            }
        }
    }
    private void update3DView() {

		new Thread(() -> {

			final int LAT = 8, LON = 7, RALT = 9, ROLL = 10, PITCH = 11, YAW = 12;

			try {
				// Directly get the arrays for each data type using dataFilter
				double[] latitudes = siFilter.getFilterData(LAT);
				double[] longitudes = siFilter.getFilterData(LON);
				double[] ralts = siFilter.getFilterData(RALT);
				double[] rolls = siFilter.getFilterData(ROLL);
				double[] pitches = siFilter.getFilterData(PITCH);
				double[] yaws = siFilter.getFilterData(YAW);

				// Find the maximum and minimum latitudes and longitudes
				double maxLat = Arrays.stream(latitudes).max().orElse(Double.MIN_VALUE) + 0.002;
				double minLat = Arrays.stream(latitudes).min().orElse(Double.MAX_VALUE) - 0.002;
				double maxLon = Arrays.stream(longitudes).max().orElse(Double.MIN_VALUE) + 0.002;
				double minLon = Arrays.stream(longitudes).min().orElse(Double.MAX_VALUE) - 0.002;

				SwingUtilities.invokeLater(() -> {
					sliderProgress.setMaximum(latitudes.length - 1);

					flightPanel.loadCoordinates(minLat, minLon, maxLat, maxLon);

					// Assuming we need to translate these arrays into a form flightPanel can use,
					// we'll create a List<Coordinate> here
					List<Coordinate> coordinates = new ArrayList<>();
			        for (int i = 0; i < latitudes.length; i++) {
			            Coordinate coordinate = new Coordinate(
			                latitudes[i],
			                longitudes[i],
			                ralts[i],
			                rolls[i],
			                pitches[i],
			                yaws[i]
			            );
			            coordinates.add(coordinate);
			        }

			        // Set the aircraft path with the generated coordinates
			        flightPanel.setAircraftPath(coordinates);

				});

			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();
		revalidate();
		repaint();
	}
  
    
}
