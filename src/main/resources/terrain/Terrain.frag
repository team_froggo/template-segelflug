#import "Common/ShaderLib/GLSLCompat.glsllib"

uniform sampler2D m_Tex1;
uniform float m_Tex1Scale;
uniform vec2 m_Offset;


varying vec2 texCoord;

void main(void)
{
	vec4 tex1 = texture2D( m_Tex1, texCoord.xy * m_Tex1Scale - m_Offset); // Tile
	gl_FragColor = tex1; 
}

