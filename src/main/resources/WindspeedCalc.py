# -*- coding: utf-8 -*-
"""
Created on Thu Jun  6 15:53:00 2024

@author: erigo
"""

import pandas as pd
import numpy as np

# CSV-Dateipfad
data = pd.read_csv('C:/Users/erigo/OneDrive/Desktop/Dokumente/Projekt2/2024-02-13 23-32-40.csv', sep=';')

# Zeilennummer, die berechnet werden soll
row_number = 245  # Beispiel: Zeile 245

# Sicherstellen, dass die eingegebene Zeilennummer gültig ist
if row_number < 0 or row_number >= len(data):
    print("Ungültige Zeilennummer.")
else:
    # Datenzeile extrahieren
    row = data.iloc[row_number]
    
    # Berechnung der Windgeschwindigkeit und -richtung direkt im Code
    ground_speed = row['Groundspeed [m/s]']
    ground_course = row['Ground course [deg]']
    air_speed = row['Airspeed [m/s]']
    yaw = row['Yaw [deg]']

    groundX = ground_speed * np.sin(np.radians(ground_course))
    groundY = ground_speed * np.cos(np.radians(ground_course))
    airX = air_speed * np.sin(np.radians(yaw))
    airY = air_speed * np.cos(np.radians(yaw))

    windX = groundX - airX
    windY = groundY - airY

    wind_speed = np.sqrt(windX**2 + windY**2)
    wind_direction = (np.degrees(np.arctan2(windY, windX)) + 360) % 360
    
    result = pd.DataFrame({
        'Zeile': [row_number],
        'Wind Strenght [m/s]': [wind_speed],
        'Wind Direction [deg]': [wind_direction]
    })
    
    print(result.to_string(index=False))
