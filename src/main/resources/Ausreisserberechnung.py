"""
Liest Daten aus CSV-Datei aus und stellt mittels Medianfilter
Ausreisser fest.

::

   Projekt     : Ausreisser
   Autor       : Nicolas Thommen
   Dateiname   : Ausreisser.py
   Beginndatum : 02.05.2024
   Enddatum    : tbd
   Version     : 1.0

"""

import pandas as pd
from scipy.signal import medfilt

# CSV-Dateipfad
dateipfad = "C:\\Users\\nicol\\OneDrive - FHNW\\Projekt 2\\Unit Testing\\Ausreisserfilter\\Ausreisser_Testfile.csv"

daten = pd.read_csv(dateipfad, usecols=lambda x: x not in [0, 1, 2], sep=';')

def apply_median_filter(data, kernel_size, threshold=None):
    filtered_data = medfilt(data, kernel_size=kernel_size)
    return filtered_data


median_filter_columns = {
    'Testspalte Medianfilter': {'kernel_size': 5, 'threshold': 5}
    }

for column, params in median_filter_columns.items():
    ausreisser_daten = apply_median_filter(daten[column], **params)
    print(f"Gefilterte Werte in {column}:", ausreisser_daten)
