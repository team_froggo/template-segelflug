# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np

# CSV-Dateipfad
data = pd.read_csv('C:/Users/erigo/OneDrive/Desktop/Dokumente/Projekt2/2024-02-13 23-32-40.csv', sep=';')

# Extraktion der Spalten
x_us = data['Time since System Start [us]'].values
y = data['Energie [m]'].values

# Benutzerdefinierte Start- und Endindizes eingeben
indexstart = 400  # Startwert einsetzen
indexend = 700   # Endwert einsetzen

# Auswahl des gewünschten Bereichs
x = x_us[indexstart:indexend]  
y = y[indexstart:indexend]  

# Polynomfit mit ausgewähltem Grad
polynom_grad = 10
koeffizienten = np.polyfit(x, y, polynom_grad)
polynom = np.poly1d(koeffizienten)

predicted_values = polynom(x)

squared_residuals = (y - predicted_values) ** 2

least_squares_error = np.sum(squared_residuals)

least_squares_error_rounded = round(least_squares_error, 3)

# Ausgabe des "Least-Squares"-Fehlers
print(least_squares_error_rounded)
