# Flight Panel Template

This template provides all necessary dependencies to build an application displaying a 3D Rendered Map including height data and flight imagery.

Use `src/main/java/app/App.java` to implement your own application. Run `src/main/java/flightpanel/FlightPanelTest.java` to see a demo in action.

![](./screenshot.png).

`pom.xml` includes the JMonkey Game Engine as well as the `json.org` JSON Library for reading and writing JSON Objects to the disk. Edit `pom.xml` for adding further Java dependencies such as Plot Libraries, Math, etc. using Artifacts provided by services such as `https://mvnrepository.com/`.

Use the coordinates (latitude, longitude) text fields in order to define the center location of an area to be loaded. Pressing `load` will fetch a number of chunks of the map based on the Swisstopo Geography survey data provided by the swisstopo HTTP `stac` api available under `https://data.geo.admin.ch/api/stac` (see the `TopoLoader.java` class for more details).

The image and height information is saved in the `./chache` directory and therefore not re-fetched every time the application is run. The `MapLoader.java` class provides the ability to install a callback for progress updates in percent.

The application has been tested against the latest JMonkeyEngine Snapshot build on Linux and Windows.

## How to use FlightPanel

Take a look at `src/main/java/flightpanel/MainFlightPanel.java` to understand how to use `FlightPanel`. 

1. Load a Map using two points spannign a rect (`lat0`, `lon0`, `lat1`, `lon1`).
2. Create a List of `Coordinate` -> `new Coordinate(lat, lon, ralt, roll, pitch, yaw)` and pass it `flightPanel.setAircaftPath)`. After this Method call, the Path is loaded into the Map View.
3. Use `flightPanel.setAircraftPosition(index)` to place the Aircraft at a certain `Coordinate` with the apropriate `roll`, `pitch` and `yaw`.


## Steps required to create a Java Project for pro2E and keep it in sync with this template project.

The goal is to create a Java base Project for Eclipse for teams participating in pro2E. This will include setting up the appropriate group and project Structure in GitLab for collaboration as well as a procedure to fetch updates from the template as the library progresses.

1. Per team create a single group on `gitlab.fhnw.ch`
2. Invite all team members to the group
3. Fork this Project under the namespace of the group
4. All team members: clone the project into your environment
5. Import the project into eclipse using the standard import project dialog
6. Open a Terminal (git bash) within the git project
7. Run the following command: `git remote add template https://gitlab.fhnw.ch/manuel.dicerbo/template-pro2e-fs24.git`. This will add a new reference named `template` to your git repository. If you run `git fetch template` then the branch `remotes/template/master` will be added to your repository (see with `git branch -a`).
8. Whenever there is progress on the template, run `git fetch template` and merge with `git merge template/master`. Then if necessary resolve all conflicts.
9. In Eclipse Rename the Project to your liking:  Right Click on the Project, Refactor, Rename.

If you already created a project prior to this then it is likely best to move all sources from the prior project to this newly cloned project.
